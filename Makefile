#
# compiler
#
CC        = g++
C	  = gcc
CXXFLAGS 		= -Wall -Wextra -std=c++2a -O3 -DNDEBUG
CFLAGS = -O3 -DNDEBUG

#
# project root
#
PRJROOT		= ./tool
LIBINC		= -I$(PRJROOT)/lib
SRC		= $(PRJROOT)/src
BASH		= $(PRJROOT)/bash-scripts
BIN		= $(PRJROOT)/bin
HOA		= -isystem$(PRJROOT)
HOA2PG	 	= $(PRJROOT)/hoa2pg
HOA2PGHDR = $(HOA2PG)/hoalexer.h $(HOA2PG)/hoaparser.h $(HOA2PG)/simplehoa.h
HOA2PGSRC = $(HOA2PG)/hoalexer.c $(HOA2PG)/hoaparser.c $(HOA2PG)/simplehoa.c $(HOA2PG)/hoa2pg.c




.PHONY: folder SImPA GIST hoa2pg size clean

TARGET = folder SImPA GIST hoa2pg size

build: $(TARGET)

folder:
	[ -d $(BIN) ] || mkdir -p $(BIN)

SImPA:
	$(CC) $(CXXFLAGS) $(LIBINC) $(HOA) $(SRC)/SImPA.cpp -o $(BIN)/SImPA

GIST:
	$(CC) $(CXXFLAGS) $(LIBINC) $(HOA) $(SRC)/GIST.cpp -o $(BIN)/GIST

hoa2pg:$(HOA2PGSRC) $(HOA2PGHDR)
	$(C) $(CFLAGS) -o $(BIN)/hoa2pg $(HOA2PGSRC)

size:
	$(CC) $(CXXFLAGS) $(LIBINC) $(HOA) $(SRC)/size.cpp -o $(BIN)/size

clean:
	rm -r -f  $(BIN)/*

benchmark-all:
	bash $(BASH)/benchmark.sh "./benchmarks/all"

benchmark-subset:
	bash $(BASH)/benchmark.sh "./benchmarks/subset"
