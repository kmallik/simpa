# !/bin/bash
cd $1
export PRJ_ROOT="./../.."
time_limit=3600

export BINS="$PRJ_ROOT/tool/bin"
export OUTPUT="$PRJ_ROOT/output"
export TABLE="$OUTPUT/table.csv"
export SIMPA="$OUTPUT/SImPA-outputs"
export GIST="$OUTPUT/GIST-outputs"


echo "Name,Number of Vertices,Number of Edges,Number of Priorities,Computation time of SImPA (in seconds),Computation time of GIST (in seconds),* if no assumption is computed by GIST">$TABLE

for f in *ehoa
do
    echo "Converting $f to pgsolver format..."
    timeout 60 $BINS/hoa2pg <$f >game.gm; retVal_hoa2pg=$?
    if [ $retVal_hoa2pg == 0 ]; then
	    echo "Conversion successful."

        echo -n $f",">>$TABLE
        $BINS/size game.gm >> $TABLE

        echo "Computing assumptions using SImPA..."
	    start=`date +%s.%N`
        timeout $time_limit $BINS/SImPA game.gm >$SIMPA/$f.txt; retVal=$?
        end=`date +%s.%N`
        runtime=$( echo "$end - $start" | bc -l )

        if [ $retVal == 124 ]; then
            echo -n ",Timeout">>$TABLE
        else
            echo -n ","$runtime>>$TABLE
        fi
        
	    echo "Computing assumptions using GIST..."
        startK=`date +%s.%N`
        timeout $time_limit $BINS/GIST game.gm >$GIST/$f.txt; retValK=$?
        endK=`date +%s.%N`
        runtimeK=$( echo "$endK - $startK" | bc -l )

        if [ $retValK == 124 ]; then
            echo ",Timeout">>$TABLE
        else
            if [ $retValK == 1 ]; then
                echo ","$runtimeK",*">>$TABLE
            else
                echo ","$runtimeK>>$TABLE
            fi
        fi
    fi
rm game.gm
done
