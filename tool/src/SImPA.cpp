/*
 * SImPA.cpp
 *
 *  A program to find maximally permissive assumptions
 *  The input is to be given as a text file */

#include <functional>
#include "Game.hpp"
#include "ParityGame.hpp"
#include "FileHandler.hpp"
// #include "hoa2pg.c"

#define NOF_VERTEX   "NOF_VERTEX"
#define VERTEX_ID   "VERTEX_ID"
#define TRANSITIONS   "TRANSITIONS"
#define NOF_COLOR   "NOF_COLOR"
#define COLORS  "COLORS"
#define SPEC_NAME     "SPEC_NAME"
#define PARITY         "PARITY"
#define BUCHI         "BUCHI"
#define COBUCHI         "COBUCHI"
#define NOF_TARGET_VERTEX     "NOF_TARGET_VERTEX"
#define TARGET_VERTICES      "TARGET_VERTICES"

/* command line inputs:
 *
 *  [1]: input file name
 *  [2]: output file name (optional, if not provided then the result is only displayed on the std I/O)
 */

int main(int argc, char* argv[]) {
    try {
        /* sanity check: input filename should be provided by the user */
        if (argc <= 1) {
            throw std::runtime_error("Too few input arguments.\n");
        }
        /* read filename */
        std::string filename(argv[1]);
        /* check if the file is a .gm (pgsolver) file */
        if(ends_with(filename, ".gm")){
            size_t N;
            std::vector<size_t> V_ID;
            std::vector<std::unordered_set<size_t>> TR;
            size_t N_COL;
            std::vector<size_t> COL;
            
            /* covert pgsolver format to normal game */
            pg2game(filename, N, V_ID, TR, N_COL, COL);

            /* construct the game */
            mpa::ParityGame G(N, V_ID, TR, N_COL, COL);

            /* compute the assumptions */
            std::multimap<size_t, size_t> unsafe_edges;
            std::vector<std::unordered_set<size_t>> condition_sets;
            std::vector<std::vector<std::multimap<size_t, size_t>>> conditional_live_group_set;
            std::multimap<size_t, size_t> depressed_edge_set;
            G.find_assumption_parity(unsafe_edges, condition_sets, conditional_live_group_set, depressed_edge_set);
            
            /* print assumptions */
            if (N_COL == 2){ /* coBuchi game */
                /* sanity check: conditional live group set is empty */
                if ((!condition_sets.empty()) || (!conditional_live_group_set.empty())){
                    std::runtime_error("run::coBuchi game is invalid");
                }
                print_unsafe_edges(unsafe_edges);
                print_depressed_edges(depressed_edge_set);
            }
            else if (N_COL == 3 && std::find(COL.begin(), COL.end(), 0) == COL.end()){ /* Buchi game */
                /* sanity check: depressed edge set and condition set is empty */
                if ((!condition_sets.empty()) || (!depressed_edge_set.empty())){
                    std::runtime_error("run::Buchi game is invalid");
                }
                print_unsafe_edges(unsafe_edges);
                print_live_groups(conditional_live_group_set[0]);
            }
            else{ /* parity game */
                print_unsafe_edges(unsafe_edges);
                print_depressed_edges(depressed_edge_set);
                print_conditional_live_groups(condition_sets, conditional_live_group_set);
            }
        }
        /* check if the file is a .hoa file */
        else if(ends_with(filename, ".ehoa")){
            mpa::ParityGame G(filename);
            
            /* compute the assumptions */
            std::multimap<size_t, size_t> unsafe_edges;
            std::vector<std::unordered_set<size_t>> condition_sets;
            std::vector<std::vector<std::multimap<size_t, size_t>>> conditional_live_group_set;
            std::multimap<size_t, size_t> depressed_edge_set;
            G.find_assumption_parity(unsafe_edges, condition_sets, conditional_live_group_set, depressed_edge_set);
            
           /* print assumptions */
            size_t N_COL = G.n_colors_;
            std::vector<size_t> COL = G.colors_;
            if (N_COL == 2){ /* coBuchi game */
                /* sanity check: conditional live group set is empty */
                if ((!condition_sets.empty()) || (!conditional_live_group_set.empty())){
                    std::runtime_error("run::coBuchi game is invalid");
                }
                print_unsafe_edges(unsafe_edges);
                print_depressed_edges(depressed_edge_set);
            }
            else if (N_COL == 3 && std::find(COL.begin(), COL.end(), 0) == COL.end()){ /* Buchi game */
                /* sanity check: depressed edge set and condition set is empty */
                if ((!condition_sets.empty()) || (!depressed_edge_set.empty())){
                    std::runtime_error("run::Buchi game is invalid");
                }
                print_unsafe_edges(unsafe_edges);
                print_live_groups(conditional_live_group_set[0]);
            }
            else{ /* parity game */
                print_unsafe_edges(unsafe_edges);
                print_depressed_edges(depressed_edge_set);
                print_conditional_live_groups(condition_sets, conditional_live_group_set);
            }
        }
        else { /* file is not a .hoa/.gm file, 
        so we read the number of vertices and vertex id, etc. directly from file */
            size_t N;
            if (!readMember(filename, N, NOF_VERTEX)) {
                throw std::runtime_error("Missing/badly entered number of vertices.\n");
            }
            std::vector<size_t> V_ID;
            if (!readVec(filename, V_ID, N, VERTEX_ID)) {
                throw std::runtime_error("Missing/badly entered player id-s of vertices.\n");
            }
            std::vector<std::unordered_set<size_t>> TR;
            if (!readVecSet(filename, TR, N, TRANSITIONS)) {
                throw std::runtime_error("Missing/badly entered transition matrix.\n");
            }
            size_t N_COL = 0;
            std::vector<size_t> COL;
            
            /* read specification from the file */
            std::string spec_name;
            if (!readMember(filename, spec_name, SPEC_NAME)) {
                throw std::runtime_error("Missing/badly entered specification name.\n");
            }
            /* For only Buchi specifications */
            if (spec_name.compare(BUCHI) == 0) {
                /* construct the game using normal constructor */
                mpa::ParityGame G(N, V_ID, TR, N_COL, COL);
                size_t nof_target_vert;
                if (!readMember(filename, nof_target_vert, NOF_TARGET_VERTEX)) {
                    throw std::runtime_error("Missing/badly entered number of target vertices for Buchi specification.\n");
                }
                std::unordered_set<size_t> target;
                if (!readSet(filename, target, nof_target_vert, TARGET_VERTICES)) {
                    throw std::runtime_error("Missing/badly entered list of target vertices for Buchi specification.\n");
                }
                
                /* compute the assumptions */
                std::multimap<size_t, size_t> unsafe_edges;
                // std::multimap<size_t, size_t> lg = {std::pair<size_t,size_t>(0,0)};
                std::vector<std::multimap<size_t, size_t>> live_group_set;
                G.find_assumption_buchi(target, unsafe_edges, live_group_set);
                
                /* print unsafe edges and live groups */
                print_unsafe_edges(unsafe_edges);
                print_live_groups(live_group_set);
            } 
            /* For only co-Buchi specifications */
            else if (spec_name.compare(COBUCHI) == 0) {
                size_t nof_target_vert;
                if (!readMember(filename, nof_target_vert, NOF_TARGET_VERTEX)) {
                    throw std::runtime_error("Missing/badly entered number of target vertices for Buchi specification.\n");
                }
                std::unordered_set<size_t> target;
                if (!readSet(filename, target, nof_target_vert, TARGET_VERTICES)) {
                    throw std::runtime_error("Missing/badly entered list of target vertices for Buchi specification.\n");
                }

                mpa::ParityGame G(N, V_ID, TR, N_COL, COL);

                /* find assumptions */
                std::multimap<size_t, size_t> unsafe_edges;
                std::multimap<size_t, size_t> depressed_edge_set;
                G.find_assumption_cobuchi(target, unsafe_edges, depressed_edge_set);
                
                /* print unsafe edges and depressed edges */
                print_unsafe_edges(unsafe_edges);
                print_depressed_edges(depressed_edge_set);
            }
            /* For only parity specifications */
            else if (spec_name.compare(PARITY) == 0) {
                if (!readMember(filename, N_COL, NOF_COLOR)) {
                    throw std::runtime_error("Missing/badly entered number of colors.\n");
                }
                if (!readVec(filename, COL, N, COLORS)) {
                    throw std::runtime_error("Missing/badly entered colors of the vertices.\n");
                }

                mpa::ParityGame G(N, V_ID, TR, N_COL, COL);

                /* compute the assumptions */
                std::multimap<size_t, size_t> unsafe_edges;
                std::vector<std::unordered_set<size_t>> condition_sets;
                std::vector<std::vector<std::multimap<size_t, size_t>>> conditional_live_group_set;
                std::multimap<size_t, size_t> depressed_edge_set;
                G.find_assumption_parity(unsafe_edges, condition_sets, conditional_live_group_set, depressed_edge_set);
                
                /* print assumptions */
                if (N_COL == 2){ /* coBuchi game */
                    /* sanity check: conditional live group set is empty */
                    if ((!condition_sets.empty()) || (!conditional_live_group_set.empty())){
                        std::runtime_error("run::coBuchi game is invalid");
                    }
                    print_unsafe_edges(unsafe_edges);
                    print_depressed_edges(depressed_edge_set);
                }
                else if (N_COL == 3 && std::find(COL.begin(), COL.end(), 0) == COL.end()){ /* Buchi game */
                    /* sanity check: depressed edge set and condition set is empty */
                    if ((!condition_sets.empty()) || (!depressed_edge_set.empty())){
                        std::runtime_error("run::Buchi game is invalid");
                    }
                    print_unsafe_edges(unsafe_edges);
                    print_live_groups(conditional_live_group_set[0]);
                }
                else{ /* parity game */
                    print_unsafe_edges(unsafe_edges);
                    print_depressed_edges(depressed_edge_set);
                    print_conditional_live_groups(condition_sets, conditional_live_group_set);
                }
            }
        }
        return 0;
    }
    /* TODO: print the same output in the output file */
    catch (const std::exception &ex) {
        std::cout << ex.what() << "\n";
        return 1;
    }
}

