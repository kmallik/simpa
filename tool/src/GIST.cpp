/*
 * GIST.cpp
 *
 *  A program to find safe and locally minimal fair assumptions
 * using the algorithm by Chatterjee et al. [2008] */

#include <functional>
#include "Game.hpp"
#include "ParityGame.hpp"
#include "StochasticParityGame.hpp"
#include "FileHandler.hpp"

#define NOF_VERTEX   "NOF_VERTEX"
#define VERTEX_ID   "VERTEX_ID"
#define TRANSITIONS   "TRANSITIONS"
#define NOF_COLOR   "NOF_COLOR"
#define COLORS  "COLORS"
#define SPEC_NAME     "SPEC_NAME"
#define PARITY         "PARITY"
#define BUCHI         "BUCHI"
#define COBUCHI         "COBUCHI"
#define NOF_TARGET_VERTEX     "NOF_TARGET_VERTEX"
#define TARGET_VERTICES      "TARGET_VERTICES"

/* command line inputs:
 *
 *  [1]: input file name
 *  [2]: output file name (optional, if not provided then the result is only displayed on the std I/O)
 */
int main(int argc, char* argv[]) {
    try {
        /* sanity check: input filename should be provided by the user */
        if (argc <= 1) {
            throw std::runtime_error("Too few input arguments.\n");
        }
        /* read filename */
        std::string filename(argv[1]);
        /* check if the file is a .hoa file */
        if(ends_with(filename, ".ehoa")){
            /* use hoa game constructor to construct the game */
            mpa::StochasticParityGame G(filename);

            /* compute the assumptions */
            std::multimap<size_t, size_t> unsafe_edges, live_edges;
            if (!G.find_unsafe_and_locally_minimal_live_edges(unsafe_edges, live_edges)) {
                std::cout << "Failed: No live edge could be found.\n";
                return 1;
            }
            /* print assumptions */
            print_unsafe_edges(unsafe_edges);
            print_live_edges(live_edges);
        }
        /* check if the file is a pgsolver file */
        else if(ends_with(filename, ".gm")){
            size_t N;
            std::vector<size_t> V_ID;
            std::vector<std::unordered_set<size_t>> TR;
            size_t N_COL;
            std::vector<size_t> COL;

            /* covert pgsolver format to normal game */
            pg2game(filename, N, V_ID, TR, N_COL, COL);

            /* construct the game */
            mpa::StochasticParityGame G(N, V_ID, TR, N_COL, COL);

            /* compute the assumptions */
            std::multimap<size_t, size_t> unsafe_edges, live_edges;
            if (!G.find_unsafe_and_locally_minimal_live_edges(unsafe_edges, live_edges)) {
                std::cout << "Failed: No live edge could be found.\n";
                return 1;
            }
            /* print assumptions */
            print_unsafe_edges(unsafe_edges);
            print_live_edges(live_edges);
        }
        else { /* file is not a .hoa/.gm file, 
        so we read the number of vertices and vertex id, etc. directly from file */
            size_t N;
            if (!readMember(filename, N, NOF_VERTEX)) {
                throw std::runtime_error("Missing/badly entered number of vertices.\n");
            }
            std::vector<size_t> V_ID;
            if (!readVec(filename, V_ID, N, VERTEX_ID)) {
                throw std::runtime_error("Missing/badly entered player id-s of vertices.\n");
            }
            std::vector<std::unordered_set<size_t>> TR;
            if (!readVecSet(filename, TR, N, TRANSITIONS)) {
                throw std::runtime_error("Missing/badly entered transition matrix.\n");
            }
            
            /* read specification from the file */
            std::string spec_name;
            if (!readMember(filename, spec_name, SPEC_NAME)) {
                throw std::runtime_error("Missing/badly entered specification name.\n");
            }
            /* For Buchi specifications */
            if (spec_name.compare(BUCHI) == 0) {
                /* construct the game using normal constructor */
                size_t nof_target_vert;
                if (!readMember(filename, nof_target_vert, NOF_TARGET_VERTEX)) {
                    throw std::runtime_error("Missing/badly entered number of target vertices for Buchi specification.\n");
                }
                std::unordered_set<size_t> target;
                if (!readSet(filename, target, nof_target_vert, TARGET_VERTICES)) {
                    throw std::runtime_error("Missing/badly entered list of target vertices for Buchi specification.\n");
                }
                
                /* convert the Buchi game to parity game */
                size_t N_COL = 3;
                std::vector<size_t> COL(N,1);
                for (auto u = target.begin(); u != target.end(); ++u){
                    COL[*u] = 2;
                }
                /* construct the game */
                mpa::StochasticParityGame G(N, V_ID, TR, N_COL, COL);

                /* compute the assumptions */
                std::multimap<size_t, size_t> unsafe_edges, live_edges;
                if (!G.find_unsafe_and_locally_minimal_live_edges(unsafe_edges, live_edges)) {
                    std::cout << "Failed: No live edge could be found.\n";
                    return 1;
                }
                /* print assumptions */
                print_unsafe_edges(unsafe_edges);
                print_live_edges(live_edges);
            }
            /* For coBuchi specifications */
            else if (spec_name.compare(COBUCHI) == 0) {
                /* construct the game using normal constructor */
                size_t nof_target_vert;
                if (!readMember(filename, nof_target_vert, NOF_TARGET_VERTEX)) {
                    throw std::runtime_error("Missing/badly entered number of target vertices for Buchi specification.\n");
                }
                std::unordered_set<size_t> target;
                if (!readSet(filename, target, nof_target_vert, TARGET_VERTICES)) {
                    throw std::runtime_error("Missing/badly entered list of target vertices for Buchi specification.\n");
                }
                
                /* convert the Buchi game to parity game */
                size_t N_COL = 2;
                std::vector<size_t> COL(N,1);
                for (auto u = target.begin(); u != target.end(); ++u){
                    COL[*u] = 0;
                }
                /* construct the game */
                mpa::StochasticParityGame G(N, V_ID, TR, N_COL, COL);

                /* compute the assumptions */
                std::multimap<size_t, size_t> unsafe_edges, live_edges;
                if (!G.find_unsafe_and_locally_minimal_live_edges(unsafe_edges, live_edges)) {
                    std::cout << "Failed: No live edge could be found.\n";
                    return 1;
                }
                /* print assumptions */
                print_unsafe_edges(unsafe_edges);
                print_live_edges(live_edges);
            }
            /* For parity specifications */
            else if (spec_name.compare(PARITY) == 0) {
                size_t N_COL;
                if (!readMember(filename, N_COL, NOF_COLOR)) {
                    throw std::runtime_error("Missing/badly entered number of colors.\n");
                }
                std::vector<size_t> COL;
                if (!readVec(filename, COL, N, COLORS)) {
                    throw std::runtime_error("Missing/badly entered colors of the vertices.\n");
                }
                /* construct the game */
                mpa::StochasticParityGame G(N, V_ID, TR, N_COL, COL);

                /* compute the assumptions */
                std::multimap<size_t, size_t> unsafe_edges, live_edges;
                if (!G.find_unsafe_and_locally_minimal_live_edges(unsafe_edges, live_edges)) {
                    std::cout << "Failed: No live edge could be found.\n";
                    return 1;
                }
                /* print assumptions */
                print_unsafe_edges(unsafe_edges);
                print_live_edges(live_edges);
            }
        }
        return 0;
    }
    /* TODO: print the same output in the output file */
    catch (const std::exception &ex) {
        std::cout << ex.what() << "\n";
        return 1;
    }
}
