#include <iostream>
#include <sstream>
#include <fstream>
#include <locale>
#include <vector>
#include <unordered_set>
#include <dirent.h>
#include <sys/stat.h>
#include <cstring>

int main(int argc, char* argv[]) {
    /* sanity check: input filename should be provided by the user */
    if (argc <= 1) {
        throw std::runtime_error("Too few input arguments.\n");
    }
    /* read filename */
    std::string filename(argv[1]);
    std::unordered_set<size_t> COL; /* initialize set of colors */
    size_t N_Edge = 0; /* initialize number of edges = 0 */
    size_t N_Vert = 0;
    std::ifstream file;
    file.open(filename);
    if (file.is_open()) {
        std::string line;
        /* go through all the lines until a match with arr_name is found */
        while(std::getline(file,line)) {
            std::stringstream line_stream(line);
            std::string name;
            line_stream >> name;
            /* check for the header line starting with "parity" */
            if (name == "parity"){
                size_t t;
                line_stream >> t;
                /* number of vertices is the number mentioned after parity + 1 */
                N_Vert = t+1;
                break; /* break the loop when header line found */
            }
        }
        for (size_t i = 0; i < N_Vert; ++i){
            /* get N+1 lines of state */
            if(std::getline(file,line)) {
                std::stringstream line_stream(line);
                /* first number is vertex id */
                size_t num;
                line_stream >> num;
                /* second number is color of that vertex */
                size_t color;
                line_stream >> color;
                /* third number is owner of that vertex */
                size_t owner_id;
                line_stream >> owner_id;
                /* update set of colors */
                COL.insert(color);
                // std::cout << "\n i=" << i << " v_id = " << id << " owner=" << owner_id << " color=" << color << "\n";
                std::string successors; /* string of successors of that vertex */
                line_stream >> successors;
                std::stringstream ss(successors);
                /* update transitions of that vertex */
                for (size_t i; ss >> i;) {
                    N_Edge += 1;   
                    // std::cout << i << ",";
                    if (ss.peek() == ',')
                        ss.ignore();
                }
            }
        }
    }
    // std::cout<<"#vertices: "<< N_Vert << "   #edges: " << N_Edge << "   #priorities: " << COL.size() <<"\n";
    std::cout << N_Vert << "," << N_Edge << "," << COL.size();
    return 1;
}