//==============================================================================
//
// Author: Kaushik Mallik
//
// An interface to the cpphoafparser library for parsing a ParityGame automaton in
// HOA format
//==============================================================================

#ifndef CPPHOAFPARSER_HOACONSUMER_BUILDPARITYGAME_H
#define CPPHOAFPARSER_HOACONSUMER_BUILDPARITYGAME_H

#include <iostream>
#include <limits>
#include <vector>
#include <unordered_set>
#include <map>
#include <queue>
#include <stack>
#include <cstring>
#include <string>

#include "cpphoafparser/consumer/hoa_consumer.hh"
#include "cpphoafparser/parser/hoa_parser.hh"

#include "Game.hpp" /* for the values of V0 and V1 */

#define UNUSED(x) (void)(x)

namespace cpphoafparser {


    struct parity_game_data {
        size_t n_vert;
        std::unordered_set<size_t> start_vert;
        std::map<size_t, std::string> ap_id;
        std::unordered_set<std::string> ap_list;
        std::vector<std::unordered_set<size_t>> tr;
        std::vector<std::unordered_set<size_t>> pre_tr;
        std::string acc_name;
        size_t n_colors = 1;
        std::vector<size_t> colors;
        std::vector<size_t> vert_id;
        unsigned int controllable_ap;
    };

    /**
 * A HOAConsumer implementation that works as an interface to convert a parity_game automaton from HOA format
 * to the format used in Mascot-SDS.
 *
 */

    class HOAConsumerBuildParityGame : public HOAConsumer {
    public:
        parity_game_data *data_;

        /** Constructor, providing a reference to the output stream */
        HOAConsumerBuildParityGame(parity_game_data *data) : data_(data), out(std::cout) {}

        virtual bool parserResolvesAliases() override {
            return false;
        }

        virtual void notifyHeaderStart(const std::string &version) override {
            UNUSED(version);
            UNUSED(out);
        }

        virtual void setNumberOfStates(unsigned int numberOfStates) override {
            data_->n_vert = numberOfStates;
            std::vector<std::unordered_set<size_t>> tr_init(numberOfStates);
            std::vector<std::unordered_set<size_t>> pre_tr_init(numberOfStates);
            std::vector<size_t> vert_id_init(numberOfStates, V1);
            std::vector<size_t> colors_init(numberOfStates);
            data_->tr = tr_init;
            data_->pre_tr = pre_tr_init;
            data_->vert_id = vert_id_init;
            data_->colors = colors_init;
        }

        virtual void addStartStates(const int_list &stateConjunction) override {
            if (stateConjunction.size() > 0) {
                for (auto u = stateConjunction.begin(); u != stateConjunction.end(); ++u){
                    data_->start_vert.insert(*u);
                }
            }
        }

        virtual void addAlias(const std::string &name, label_expr::ptr labelExpr) override {
            UNUSED(name);
            UNUSED(labelExpr);
        }

        virtual void setAPs(const std::vector<std::string> &aps) override {
            data_->ap_id.clear();
            for (size_t i = 0; i < aps.size(); i++) {
                data_->ap_id.insert({i, aps[i]});
                data_->ap_list.insert(aps[i]);
            }
        }

        virtual void setAcceptanceCondition(unsigned int numberOfSets, acceptance_expr::ptr accExpr) override {
            // for (size_t i = 0; i < numberOfSets; i++) {
            //     std::vector<size_t> acc_label;
            //     data_->acc_signature.push_back(acc_label);
            // }
            // std::stack<acceptance_expr::ptr> nodes;
            // nodes.push(accExpr);
            // while (nodes.size() != 0) {
            //     acceptance_expr::ptr curr_node = nodes.top();
            //     nodes.pop();
            //     if (curr_node->isOR()) {
            //         nodes.push(curr_node->getLeft());
            //         nodes.push(curr_node->getRight());
            //     } else if (curr_node->isAND()) {
            //         std::array<size_t, 2> pair;
            //         acceptance_expr::ptr lchild = curr_node->getLeft();
            //         acceptance_expr::ptr rchild = curr_node->getRight();
            //         if (!lchild->isAtom() || !rchild->isAtom()) {
            //             throw std::runtime_error("not a valid parity_game specification.");
            //         } else {
            //             if (lchild->getAtom().getType() == AtomAcceptance::TEMPORAL_FIN) {
            //                 pair[1] = lchild->getAtom().getAcceptanceSet();
            //                 if (rchild->getAtom().getType() != AtomAcceptance::TEMPORAL_INF) {
            //                     throw std::runtime_error("not a valid parity_game specification.");
            //                 } else {
            //                     pair[0] = rchild->getAtom().getAcceptanceSet();
            //                 }
            //             } else if (lchild->getAtom().getType() == AtomAcceptance::TEMPORAL_INF) {
            //                 pair[0] = lchild->getAtom().getAcceptanceSet();
            //                 if (rchild->getAtom().getType() != AtomAcceptance::TEMPORAL_FIN) {
            //                     throw std::runtime_error("not a valid parity_game specification.");
            //                 } else {
            //                     pair[1] = rchild->getAtom().getAcceptanceSet();
            //                 }
            //             } else {
            //                 throw std::runtime_error("not a valid parity_game specification.");
            //             }
            //         }
            //         data_->acc_pairs.push_back(pair);
            //     }
            // }
            UNUSED(numberOfSets);
            UNUSED(accExpr);
        }

        virtual void provideAcceptanceName(const std::string &name, const std::vector<IntOrString> &extraInfo) override {
            data_->acc_name = name;
            UNUSED(extraInfo);
        }

        virtual void setName(const std::string &name) override {
            UNUSED(name);
        }

        virtual void setTool(const std::string &name, std::shared_ptr<std::string> version) override {
            UNUSED(name);
            UNUSED(version);
        }

        virtual void addProperties(const std::vector<std::string> &properties) override {
            UNUSED(properties);
        }

        virtual void addMiscHeader(const std::string &name, const std::vector<IntOrString> &content) override {
            if (strcmp(name.c_str(), "controllable-AP")) {
                throw std::runtime_error("The set controllable-AP is not provided");
            } else if (content.size() == 0){
                throw std::runtime_error("The set controllable-AP is not provided");
            } else if (content.size() > 1){
                throw std::runtime_error("The set controllable-AP should be singleton");
            } else {
                data_->controllable_ap = content[0].getInteger();
            }
        }

        virtual void notifyBodyStart() override {}

        virtual void addState(unsigned int id,
                              std::shared_ptr<std::string> info,
                              label_expr::ptr labelExpr,
                              std::shared_ptr<int_list> accSignature) override {
            if (!accSignature) {
                throw std::runtime_error("no color specified for a state");
            }
            std::vector<unsigned int> accSignatureValue = *accSignature.get();
            if (accSignatureValue.size() == 0) {
                throw std::runtime_error("no color specified for a state");
            } else if (accSignatureValue.size() > 1) {
                throw std::runtime_error("multiple color not allowed");
            }
            data_->colors[id] = accSignatureValue[0];
            if (accSignatureValue[0] >= data_->n_colors){
                data_->n_colors = accSignatureValue[0]+1;
            }
            if (data_->vert_id[id] == V0){
                UNUSED(labelExpr);
            } else{
                std::stack<label_expr::ptr> nodes;
                nodes.push(labelExpr);
                while (nodes.size() != 0) {
                    label_expr::ptr curr_node = nodes.top();
                    nodes.pop();
                    if (curr_node->isAND()) {
                        nodes.push(curr_node->getLeft());
                        nodes.push(curr_node->getRight());
                    } else if (curr_node->isNOT()){
                        nodes.push(curr_node->getLeft());
                    } else if (curr_node->isAtom()){
                        unsigned int atom_id = curr_node->getAtom().getAPIndex();
                        if (atom_id == data_->controllable_ap){
                            data_->vert_id[id] = V0;
                            break;
                        }
                    }
                }
            }
            UNUSED(info);
        }

        virtual void addEdgeImplicit(unsigned int stateId,
                                     const int_list &conjSuccessors,
                                     std::shared_ptr<int_list> accSignature) override {
            UNUSED(stateId);
            UNUSED(conjSuccessors);
            UNUSED(accSignature);
        }

        virtual void addEdgeWithLabel(unsigned int stateId,
                                      label_expr::ptr labelExpr,
                                      const int_list &conjSuccessors,
                                      std::shared_ptr<int_list> accSignature) override {


            for (auto u = conjSuccessors.begin(); u != conjSuccessors.end(); ++u){
                data_->tr[stateId].insert(*u);
                data_->pre_tr[*u].insert(stateId);
            }
            if (!labelExpr || data_->vert_id[stateId] == V0){
                UNUSED(labelExpr);
            } else{
                std::stack<label_expr::ptr> nodes;
                nodes.push(labelExpr);
                while (nodes.size() != 0) {
                    label_expr::ptr curr_node = nodes.top();
                    nodes.pop();
                    if (curr_node->isAND()) {
                        nodes.push(curr_node->getLeft());
                        nodes.push(curr_node->getRight());
                    } else if (curr_node->isNOT()){
                        nodes.push(curr_node->getLeft());
                    } else if (curr_node->isAtom()){
                        unsigned int atom_id = curr_node->getAtom().getAPIndex();
                        if (atom_id == data_->controllable_ap){
                            data_->vert_id[stateId] = V0;
                            break;
                        }
                    }
                }
            }
            UNUSED(accSignature);
        }

        virtual void notifyEndOfState(unsigned int stateId) override {
            UNUSED(stateId);
        }

        virtual void notifyEnd() override {}

        virtual void notifyAbort() override {}

        virtual void notifyWarning(const std::string &warning) override {
            std::cerr << "Warning: " << warning << std::endl;
        }

    private:
        /** Reference to the output stream */
        std::ostream &out;
    };

}// namespace cpphoafparser

#endif
