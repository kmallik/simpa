/*
 * Class: StochasticGame
 *
 * Class formalizing a stochastic two player parity game
 */

#ifndef STOCHASTICGAME_HPP_
#define STOCHASTICGAME_HPP_

#include <map>
#include "Game.hpp"
#include "ParityGame.hpp"

#define VR 2 /* random vertices */

namespace mpa {
class StochasticParityGame: public ParityGame {
public:
    /* same constructor as ParityGame, except that now VR is allowed as vertex id of random vertices */
    StochasticParityGame(size_t n_vert,
                   std::vector<size_t> vert_id,
                   std::vector<std::unordered_set<size_t>> tr,
                   size_t n_colors,
                   std::vector<size_t> colors): ParityGame(n_vert, vert_id, tr, n_colors, colors) {
        /* sanity check for vertex id-s */
        for (auto i = vert_id_.begin(); i != vert_id_.end(); ++i) {
            if ((*i != V0) && (*i != V1) && (*i != VR)) {
                std::cerr << "[WARNING] Invalid assignment of vertices to Player 0, Player 1, and random vertices.\n";
            }
        }
        // /* populate pre_tr_ */
        // for (size_t i = 0; i < n_vert_; i++) {
        //     std::unordered_set<size_t> s;
        //     pre_tr_.push_back(s);
        // }
        // for (size_t u = 0; u < n_vert_; u++) {
        //     for (auto v = tr_[u].begin(); v != tr_[u].end(); ++v) {
        //         pre_tr_[*v].insert(u);
        //     }
        // }
    }
    /* constructor: StochasticParityGame
    * constructor from a HOA file */
    StochasticParityGame(const std::string filename): ParityGame(filename) {}

    // /* copy constructor */
    // StochasticParityGame(const StochasticParityGame& other): ParityGame(other): Game(other) {
    //     n_vert_ = other.n_vert_;
    //     vert_id_ = other.vert_id_;
    //     tr_ = other.tr_;
    //     pre_tr_ = other.pre_tr_;
    //     n_colors_ = other.n_colors_;
    //     colors_ = other.colors_;
    // }

    /* solve (qualtiative) stochastic parity game
     * via reduction to 2-player parity game [Chatterjee et al. 2003] */
    std::unordered_set<size_t> solve_stochastic_parity_game() const {
        ParityGame game_copy(*this);
        size_t n_vert = game_copy.n_vert_;
        /* convert max-parity to min-parity */
        size_t max_color = game_copy.n_colors_-1;
        if (max_color % 2 == 1){
            max_color += 1;
        }
        // std::cout << "max to min; max_col = " << max_color << "\n";
        for (size_t v = 0; v < game_copy.n_vert_; v++) {
            // std::cout << " vertex "<< v << ": before " << game_copy.colors_[v];
            game_copy.colors_[v] = max_color - game_copy.colors_[v];
            // std::cout << ", after " << game_copy.colors_[v] <<"\n";
        }
        /* replace every random vertex with the gadget shown in [Chatterjee et al. 2003] */
        for (size_t u = 0; u < n_vert; u++) {
            if (game_copy.vert_id_[u] == VR) {
                game_copy.vert_id_[u] = V1; /* the random vertex itself gets relabeled as Player 1 vertex, the color remains the same */
                /* delete all the outgoing edges from the vertex */
                for (auto v = game_copy.tr_[u].begin(); v != game_copy.tr_[u].end(); ++v) {
                    game_copy.pre_tr_[*v].erase(u);
                }
                game_copy.tr_[u].clear();
                /* add children and grandchildren */
                for (size_t k = 0; k <= (game_copy.colors_[u] + 1); k = k + 2) {
                    game_copy.append_vertex(V0, {u}, {}, game_copy.colors_[u]);
                    size_t this_vert = game_copy.n_vert_ - 1;
                    if (k > 0) {
                        game_copy.append_vertex(V0, {this_vert}, tr_[u], k - 1);
                    }
                    if (k <= game_copy.colors_[u]) {
                        game_copy.append_vertex(V1, {this_vert}, tr_[u], k);
                    }
                }
            }
        }
        /* convert min-parity to max-parity */
        // std::cout << "opposite min to max \n";
        for (size_t v = 0; v < game_copy.n_vert_; v++) {
            // std::cout << " vertex "<< v << ": before " << game_copy.colors_[v];
            game_copy.colors_[v] = max_color - game_copy.colors_[v];
            // std::cout << ", after " << game_copy.colors_[v] <<"\n";
        }
        std::unordered_set<size_t> windom = game_copy.solve_parity_game();
        /* only keep those which are part of the old game graph */
        for (size_t v = 0; v < game_copy.n_vert_; v++) {
            if (v >= n_vert_) {
                windom.erase(v);
            }
        }
        // std::cout << "total vertices: " << n_vert_ << "\n";
        // print_set("windom",windom);
        return windom;
    }
    /* compute the set of safety and locally minimal strongly fair assumption (set of live edges) using the algorithm by Chatterjee et al. [2008] */
    bool find_unsafe_and_locally_minimal_live_edges(std::multimap<size_t, size_t>& unsafe_edges, std::multimap<size_t, size_t>& live_edges) const {
        /* compute unsafe edges */
        unsafe_edges.clear();
        std::unordered_set<size_t> maybe_winning_region;
        find_unsafe_edges_parity(maybe_winning_region, unsafe_edges);
        /* remove the surely losing region and continue with only the maybe winning region */
        std::unordered_set<size_t> surely_losing_region = set_complement(maybe_winning_region);
        StochasticParityGame game_copy(*this);
        game_copy.remove_vertices(surely_losing_region);
        /* compute locally minimal set of live edges */
        live_edges.clear();
        /* assume that every player 1 edge is live */
        for (size_t u = 0; u < n_vert_; u++) {
            if (vert_id_[u] == V0) continue;
            for (auto v = game_copy.tr_[u].begin(); v != game_copy.tr_[u].end(); ++v) {
                live_edges.insert(std::pair<size_t, size_t>(u, *v));
            }
        }
        /* if the maybe winning region is not same as the conditional sure winning region, then stop */
        std::unordered_set<size_t> set = game_copy.compute_conditional_sure_winning_region(live_edges);
        if (set.size() != maybe_winning_region.size()) { /* we assumed the invariant that "set" is a subset of "maybe_winning_region," and so the set equivalence can be checked by counting number of elements */
            return false;
        }
        /* the following loop one-by-one removes live edges which do not contribute to winning */
        bool converged;
        do {
            converged = true;
            for (auto e = live_edges.begin(); e != live_edges.end();) {
                std::multimap<size_t, size_t> live_edges_copy = live_edges;
                // std::cout << e->first << e->second << "\n";
                // remove_pair(live_edges_copy, *e);
                for (auto f = live_edges_copy.begin(); f != live_edges_copy.end(); ++f) {
                    if ((e->first == f->first) && (e->second == f->second)) {
                        auto it = f;
                        live_edges_copy.erase(it);
                        // std::cout << "removed:";
                        break;
                    }
                }

                set = game_copy.compute_conditional_sure_winning_region(live_edges_copy);
                if (set.size() == maybe_winning_region.size()) {
                    // std::cout << "success \n";
                    auto it = e;
                    ++e;
                    live_edges.erase(it);
                    converged = false;
                } else {
                    // std::cout<< "failed \n";
                    ++e;
                }
                // std::cout << set.size() << "!=" << maybe_winning_region.size() << "\n";
            }
        } while(!converged);
        return true;
    }

    /* compute the sure winning region given live edges
     * given that every vertex is maybe winning
     * (uses a reduction to stochastic game as outlined in Chatterjee et al. [2008]) */
    std::unordered_set<size_t> compute_conditional_sure_winning_region(std::multimap<size_t, size_t> live_edges) {
        StochasticParityGame game_new(n_vert_, vert_id_, tr_, n_colors_, colors_);
        for (size_t u = 0; u < n_vert_; u++) {
            if (live_edges.contains(u)) { /* u is source to live edge(s) */
                game_new.vert_id_[u] = VR;
                if (live_edges.count(u) < game_new.tr_[u].size()) { /* some edges from u are not live */
                    std::unordered_set<size_t> live_succ;
                    // typedef std::multimap<size_t, size_t>::iterator multimap_iterator;
                    auto values = live_edges.equal_range(u);
                    for (auto it = values.first; it != values.second; it++) {
                            live_succ.insert(it->second);
                    }
                    // std::cout << u << "-->";
                    // print_set("live",live_succ);
                    std::unordered_set<size_t> non_live_succ = set_difference(game_new.tr_[u], live_succ);
                    game_new.remove_outgoing_edges(u, non_live_succ);
                    non_live_succ.insert(u);
                    game_new.append_vertex(V1, {u}, non_live_succ, colors_[u]);
                } else if (live_edges.count(u) > game_new.tr_[u].size()){
                    std::runtime_error("ParityGame::compute_conditional_sure_winning_region: some live edge is not a valid edge.");
                }
            }
        }
        std::unordered_set<size_t> windom = game_new.solve_stochastic_parity_game();
        /* remove the copies of vertices created during the reduction */
        for (auto v = windom.begin(); v!= windom.end(); ) {
            if (*v < n_vert_) {
                ++v;
            } else {
                v = windom.erase(v);
            }
        }
        return windom;
    }
}; /* close class definition */
} /* close namespace */

#endif
