/* FileHandler.hpp
 *
 *  Created by: Kaushik
 *  Date: 12/11/2019 */

#include <iostream>
#include <sstream>
#include <fstream>
#include <locale>
#include <vector>
#include <unordered_set>
#include <dirent.h>
#include <sys/stat.h>
#include <cstring>

/**
 *  @brief Some basic operations for reading inputs and writing output.
 */

 /*! Ensures that a specified subdirectory exists.
  *  \param[in]  dirName     Name of the desired subdirectory.
  */
 template <class dir_type>
 void checkMakeDir(dir_type dirName) {
     DIR* dir = opendir(dirName);
     if (dir) {
         closedir(dir);
     }
     else if (ENOENT == errno) {
         int result = mkdir(dirName, 0777);
         (void) result;
     }
 }

/*! Read a member from a file. (A member is an attribute whose value is a scalar.)
 * \param[in] filename  Name of the file
 * \param[in] member_value  Reference to the variable that will contain the read member value
 * \param[in] member_name     The name of the member whose value is to be read
 * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
template<class T>
int readMember(const std::string& filename, T& member_value, const std::string& member_name) {
    std::ifstream file;
    file.open(filename);
    std::string line;
    if (file.is_open()) {
        while(std::getline(file,line)) {
            /* check if a match is found with the member_name */
            if(line.find(member_name)!=std::string::npos) {
                /* the data is in the next line*/
                if(std::getline(file,line)) {
                  std::istringstream stream(line);
                  stream >> member_value;
                } else {
                    try {
                        throw std::runtime_error("FileHandler:readMember: Unable to read data member");
                    } catch (std::exception &e) {
                        std::cout << e.what() << "\n";
                        return 0;
                    }
                }
                file.close();
                return 1;
            }
        }
        /* while loop exited and data member was not found */
        try {
            throw std::runtime_error("FileHandler:readMember: Member not found.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    } else {
        try {
            throw std::runtime_error("FileHandler:readMember: Unable to open input file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}

// /*! Read 1-dimensional vector from file
//  * \param[in] filename  Name of the file
//  * \param[in] v                  Reference to the vector that will contain the read vector value
//  * \param[in] no_elem     The size of the vector
//  * \param[in] vec_name     The name of the vector whose value is to be read
//  * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
// template<class T>
// int readVec(const std::string& filename, std::vector<T>& v, size_t no_elem, const std::string& vec_name) {
//     std::ifstream file;
//     file.open(filename);
//     if (file.is_open()) {
//         std::string line;
//         while(std::getline(file,line)) {
//             if(line.find(vec_name)!=std::string::npos) {
//                 for (size_t i=0; i<no_elem; i++) {
//                     if(std::getline(file,line)) {
//                         std::stringstream stream(line);
//                         std::locale loc;
//                         if (!std::isdigit(line[0],loc)) {
//                             try {
//                                 throw std::runtime_error("FileHandler:readVec: Number of rows do not match with number of elements.");
//                             } catch (std::exception &e) {
//                                 std::cout << e.what() << "\n";
//                                 return 0;
//                             }
//                         } else {
//                             T val;
//                             stream >> val;
//                             v.push_back(val);
//                         }
//                     } else {
//                         try {
//                             throw std::runtime_error("FileHandler:readVec: Unable to read vector.");
//                         } catch (std::exception &e) {
//                             std::cout << e.what() << "\n";
//                             return 0;
//                         }
//                     }
//                 }
//                 file.close();
//                 return 1;
//             }
//         }
//         /* while loop exited and the vec was not found */
//         try {
//             throw std::runtime_error("FileHandler:readVec: Vector not found.");
//         } catch (std::exception &e) {
//             std::cout << e.what() << "\n";
//             return 0;
//         }
//     } else {
//         try {
//             throw std::runtime_error("FileHandler:readVec: Unable to open input file.");
//         } catch (std::exception &e) {
//             std::cout << e.what() << "\n";
//             return 0;
//         }
//     }
// }

/*! Read 1-dimensional vector from file
 * \param[in] filename  Name of the file
 * \param[in] v                  Reference to the vector that will contain the read vector value
 * \param[in] no_elem     The size of the vector
 * \param[in] vec_name     The name of the vector whose value is to be read
 * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
template<class T>
int readVec(const std::string& filename, std::vector<T>& vec, size_t no_elem, const std::string& vec_name) {
    std::ifstream file;
    file.open(filename);
    if (file.is_open()) {
        std::string line;
        /* go through all the lines until a match with arr_name is found */
        while(std::getline(file,line)) {
            if(line.find(vec_name)!=std::string::npos) {
                for (size_t i=0; i<no_elem; i++) {
                    if(std::getline(file,line)) {
                        std::stringstream line_stream(line);
                        /* first character should be the index of the vector element */
                        size_t index;
                        line_stream >> index;
                        // if (index >= no_elem) {
                        //     try {
                        //         throw std::runtime_error("FileHandler:readVecSet: Index out of bound.");
                        //     } catch (std::exception &e) {
                        //         std::cout << e.what() << "\n";
                        //         return 0;
                        //     }
                        // }
                        // /* add empty elements for all the missing indices */
                        // for (size_t j = i; j < index; j++, i++) {
                        //     T t;
                        //     vec.push_back(t);
                        // }
                        if (index != i) {
                            try {
                                throw std::runtime_error("FileHandler:readVec: Missing index.");
                            } catch (std::exception &e) {
                                std::cout << e.what() << "\n";
                                return 0;
                            }
                        }
                        /* ignore all the characters until a colon is seen */
                        line_stream.ignore(std::numeric_limits<std::streamsize>::max(), ':');
                        /* add the next element and ignore the rest of the line */
                        T t;
                        line_stream >> t;
                        vec.push_back(t);
                    } else {
                        try {
                            throw std::runtime_error("FileHandler:readVec: Unable to read vector.");
                        } catch (std::exception &e) {
                            std::cout << e.what() << "\n";
                            return 0;
                        }
                    }
                }
                file.close();
                return 1;
            }
        }
        /* while loop exited and the vec was not found */
        try {
            throw std::runtime_error("FileHandler:readVecSet: Vector not found.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    } else {
        try {
            throw std::runtime_error("FileHandler:readVecSet: Unable to open input file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}

/*! Read 1-dimensional integer set (unordered) from file
 * NOTE: NO LINE BREAK IS ALLOWED!
 * \param[in] filename  Name of the file
 * \param[in] s                  Reference to the set that will contain the read set value
 * \param[in] no_elem     The size of the set
 * \param[in] set_name     The name of the set whose value is to be read
 * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
template<class T>
int readSet(const std::string& filename, std::unordered_set<T>& s, size_t no_elem, const std::string& set_name) {
    std::ifstream file;
    file.open(filename);
    if (file.is_open()) {
        std::string line;
        while(std::getline(file,line)) {
            if(line.find(set_name)!=std::string::npos) {
                for (size_t i=0; i<no_elem; i++) {
                    if(std::getline(file,line)) {
                        std::stringstream stream(line);
                        std::locale loc;
                        if (!std::isdigit(line[0],loc)) {
                            try {
                                throw std::runtime_error("FileHandler:readSet: Number of rows do not match with number of elements.");
                            } catch (std::exception &e) {
                                std::cout << e.what() << "\n";
                                return 0;
                            }
                        } else {
                            T val;
                            stream >> val;
                            s.insert(val);
                        }
                    } else {
                        try {
                            throw std::runtime_error("FileHandler:readSet: Unable to read set.");
                        } catch (std::exception &e) {
                            std::cout << e.what() << "\n";
                            return 0;
                        }
                    }
                }
                file.close();
                return 1;
            }
        }
        /* while loop exited and the set was not found */
        try {
            throw std::runtime_error("FileHandler:readSet: Set not found.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    } else {
        try {
            throw std::runtime_error("FileHandler:readSet: Unable to open input file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}

/*! Read vector of pointers to unordered sets (can be thought of as a 2-d table) from file
 *  NOTE: NO LINE BREAK IS ALLOWED!
 * \param[in] filename  Name of the file
 * \param[in] vec             Reference to the vector that will contain the read vector value
 * \param[in] no_elem     The size of the vector
 * \param[in] vec_name     The name of the vector whose value is to be read
 * \param[out] out_flag          A flag that is 0 when reading was unsuccessful, and is 1 when reading was successful*/
template<class T>
int readVecSet(const std::string& filename, std::vector<std::unordered_set<T>>& vec, size_t no_elem, const std::string& vec_name) {
    std::ifstream file;
    file.open(filename);
    if (file.is_open()) {
        std::string line;
        /* go through all the lines until a match with arr_name is found */
        while(std::getline(file,line)) {
            if(line.find(vec_name)!=std::string::npos) {
                for (size_t i=0; i<no_elem; i++) {
                    if(std::getline(file,line)) {
                        if (line == "") { /* the line is empty */
                            /* no more members, fill the remaining positions with empty sets */
                            for (size_t j = i; j < no_elem; j++, i++) {
                                std::unordered_set<T> set;
                                vec.push_back(set);
                            }
                        } else {
                            std::stringstream line_stream(line);
                            /* first character should be the index of the vector element */
                            size_t index;
                            line_stream >> index;
                            if (index >= no_elem) {
                                try {
                                    throw std::runtime_error("FileHandler:readVecSet: Index out of bound.");
                                } catch (std::exception &e) {
                                    std::cout << e.what() << "\n";
                                    return 0;
                                }
                            }
                            if (index < i) {
                                try {
                                    throw std::runtime_error("FileHandler:readVecSet: Repeated index.");
                                } catch (std::exception &e) {
                                    std::cout << e.what() << "\n";
                                    return 0;
                                }
                            }
                            /* add empty sets for all the missing indices */
                            for (size_t j = i; j < index; j++, i++) {
                                std::unordered_set<T> set;
                                vec.push_back(set);
                            }
                            /* ignore all the characters until a colon is seen */
                            line_stream.ignore(std::numeric_limits<std::streamsize>::max(), ':');
                            /* create a set for the current index */
                            std::unordered_set<T> set;
                            while (line_stream.good()) {
                                T x;
                                line_stream >> x;
                                if (!line_stream.fail()) {
                                    set.insert(x);
                                }
                            }
                            vec.push_back(set);
                        }
                    } else {
                        try {
                            throw std::runtime_error("FileHandler:readVecSet: Unable to read vector.");
                        } catch (std::exception &e) {
                            std::cout << e.what() << "\n";
                            return 0;
                        }
                    }
                }
                file.close();
                return 1;
            }
        }
        /* while loop exited and the vec was not found */
        try {
            throw std::runtime_error("FileHandler:readVecSet: Vector not found.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    } else {
        try {
            throw std::runtime_error("FileHandler:readVecSet: Unable to open input file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}
/*! Create a file OR erase previous data written to a file
 * \param[in] filename  The name of the file*/
void create(const std::string& filename) {
    std::ofstream file;
    file.open(filename, std::ofstream::out | std::ofstream::trunc);
    if (file.is_open()) {
        file.close();
    }
}
/* some functions for writing data to file */
/*! Write a member to a file. (A member is an attribute whose value is a scalar.)
 * \param[in] filename  Name of the file
 * \param[in] member_name     The name of the member
 * \param[in] member_value   The value of the member
 * \param[in] mode                     [Optional] The writing mode: "a" for append, "w" for write/overwrite. Default="a".*/
template<class T>
void writeMember(const std::string& filename, const std::string& member_name, T member_value, const char* mode="a") {
    std::ofstream file;
    if (!strcmp(mode,"a")) {
        file.open(filename, std::ios_base::app);
    } else if (!strcmp(mode,"w")) {
        file.open(filename, std::ios_base::out);
    } else {
        try {
            throw std::runtime_error("FileHandler:writeMember: Invalid mode.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }

    if (file.is_open()) {
        file << "# " << member_name << "\n";
        file << member_value << "\n";
        file.close();
    } else {
        try {
            throw std::runtime_error("FileHandler:writeMember: Unable to open output file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
}

/*! Write 1-dimensional integer vector to file
 * \param[in] filename  Name of the file
 * \param[in] vec_name     The name of the vector to be written
 * \param[in] v                     The vector
 * \param[in] mode              [Optional] The writing mode: "a" for append, "w" for write/overwrite. Default="a".*/
template<class T>
void writeVec(const std::string& filename, const std::string& vec_name, std::vector<T>& v, const char* mode="a") {
    std::ofstream file;
    if (!strcmp(mode,"a")) {
        file.open(filename, std::ios_base::app);
    } else if (!strcmp(mode,"w")) {
        file.open(filename, std::ios_base::out);
    } else {
        try {
            throw std::runtime_error("FileHandler:writeVec: Invalid mode.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
    if (file.is_open()) {
        file << "# " << vec_name << "\n";
        for (size_t i=0; i<v.size(); i++) {
            file << v[i] << "\n";
        }
        file.close();
    } else {
        try {
            throw std::runtime_error("FileHandler:writeVec: Unable to open output file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
}

/*! Write 1-dimensional integer set (unordered) to file
 * \param[in] filename  Name of the file
 * \param[in] set_name     The name of the set
 * \param[in] s                    The set
 * \param[in] mode              [Optional] The writing mode: "a" for append, "w" for write/overwrite. Default="a".*/
template<class T>
void writeSet(const std::string& filename, const std::string& set_name, std::unordered_set<T>& s, const char* mode="a") {
    std::ofstream file;
    if (!strcmp(mode,"a")) {
        file.open(filename, std::ios_base::app);
    } else if (!strcmp(mode,"w")) {
        file.open(filename, std::ios_base::out);
    } else {
        try {
            throw std::runtime_error("FileHandler:writeSet: Invalid mode.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
    if (file.is_open()) {
        file << "# " << set_name << "\n";
        for (auto i=s.begin(); i!=s.end(); ++i) {
            file << *i << "\n";
        }
        file.close();
    } else {
        try {
            throw std::runtime_error("FileHandler:writeSet: Unable to open output file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
}

/*! Write vector of references to unordered sets (can be thought of as a 2-d table) to file
 * \param[in] filename  Name of the file
 * \param[in] vec_name     The name of the vector
 * \param[in] vec                The vector
 * \param[in] mode                     [Optional] The writing mode: "a" for append, "w" for write/overwrite. Default="a".*/
template<class T>
void writeVecSet(const std::string& filename, const std::string& vec_name, std::vector<std::unordered_set<T>*> vec, const char* mode="a") {
    std::ofstream file;
    if (!strcmp(mode,"a")) {
        file.open(filename, std::ios_base::app);
    } else if (!strcmp(mode,"w")) {
        file.open(filename, std::ios_base::out);
    } else {
        try {
            throw std::runtime_error("FileHandler:writeVecSet: Invalid mode.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
    if (file.is_open()) {
        file << "# " << vec_name << "\n";
        for (size_t i=0; i<vec.size(); i++) {
            if (vec[i]->size()==0) {
                file << "x\n";
            } else {
                for (auto it=vec[i]->begin(); it!=vec[i]->end(); it++) {
                    file << (*it) << " ";
                }
                file << "\n";
            }
        }
        file.close();
    } else {
        try {
            throw std::runtime_error("FileHandler:writeVecSet: Unable to open output file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
        }
    }
}

// /*! read a game in pgsolver format from a file and convert it to normal game
//  * \param[in] filename  Name of the file
//  * \param[out] N     Number of vertices
//  * \param[out] V_ID  Vector containing owner of the vertices
//  * \param[out] TR    Transition vector
//  * \param[out] N_COL Number of colors
//  * \param[out] COL   Vector of colors */
// int pg2game(const std::string& filename,
//                 size_t& N,
//                 std::vector<size_t>& V_ID,
//                 std::vector<std::unordered_set<size_t>>& TR,
//                 size_t& N_COL,
//                 std::vector<size_t>& COL){
//     N_COL = 0; /* initialize number of colors = 0 */
//     std::ifstream file;
//     file.open(filename);
//     if (file.is_open()) {
//         std::string line;
//         /* go through all the lines until a match with arr_name is found */
//         while(std::getline(file,line)) {
//             std::stringstream line_stream(line);
//             std::string name;
//             line_stream >> name;
//             /* check for the header line starting with "parity" */
//             if (name == "parity"){
//                 size_t t;
//                 line_stream >> t;
//                 /* number of vertices is the number mentioned after parity + 1 */
//                 N = t+1;
//                 // std::cout << "\n N = " << N;
//                 /* initialize all vectors to a vector of length N */
//                 std::vector<size_t> vec_init(N,0);
//                 std::vector<std::unordered_set<size_t>> TR_init(N);
//                 COL = vec_init;
//                 V_ID = vec_init;
//                 TR = TR_init;
//                 break; /* break the loop when header line found */
//             }
//         }
//         for (size_t i = 0; i < N; ++i){
//             /* get N+1 lines of state */
//             if(std::getline(file,line)) {
//                 std::stringstream line_stream(line);
//                 /* first number is vertex id */
//                 size_t id;
//                 line_stream >> id;
//                 /* second number is color of that vertex */
//                 size_t color;
//                 line_stream >> color;
//                 /* third number is owner of that vertex */
//                 size_t owner_id;
//                 line_stream >> owner_id;
//                 /* set the color and owner of that vertex */
//                 V_ID[id] = owner_id;
//                 COL[id] = color;
//                 /* update number of color if color of this vertex is more than N_COL */
//                 if (color >= N_COL)
//                     N_COL = color + 1;
//                 std::cout << "\n i=" << i << " v_id = " << id << " owner=" << owner_id << " color=" << color << "\n";
//                 std::string successors; /* string of successors of that vertex */
//                 line_stream >> successors;
//                 std::stringstream ss(successors);
//                 /* update transitions of that vertex */
//                 for (size_t i; ss >> i;) {
//                     TR[id].insert(i);   
//                     std::cout << i << ",";
//                     if (ss.peek() == ',')
//                         ss.ignore();
//                 }
//             }else {
//                 try {
//                     throw std::runtime_error("FileHandler:pg2game: Unable to read state-transitions lines.");
//                 } catch (std::exception &e) {
//                     std::cout << e.what() << "\n";
//                     return 0;
//                 }
//             }
//         }
//         file.close();
//         return 1;
//     } else {
//         try {
//             throw std::runtime_error("FileHandler:pg2game: Unable to open input file.");
//         } catch (std::exception &e) {
//             std::cout << e.what() << "\n";
//             return 0;
//         }
//     }
// }

/*! read a game in pgsolver format from a file and convert it to normal game
 * \param[in] filename  Name of the file
 * \param[out] N     Number of vertices
 * \param[out] V_ID  Vector containing owner of the vertices
 * \param[out] TR    Transition vector
 * \param[out] N_COL Number of colors
 * \param[out] COL   Vector of colors */
int pg2game(const std::string& filename,
                size_t& N,
                std::vector<size_t>& V_ID,
                std::vector<std::unordered_set<size_t>>& TR_final,
                size_t& N_COL,
                std::vector<size_t>& COL){
    std::vector<size_t> V_name;
    std::vector<std::unordered_set<size_t>> TR;
    N_COL = 0; /* initialize number of colors = 0 */
    std::ifstream file;
    file.open(filename);
    if (file.is_open()) {
        std::string line;
        /* go through all the lines until a match with arr_name is found */
        while(std::getline(file,line)) {
            std::stringstream line_stream(line);
            std::string name;
            line_stream >> name;
            /* check for the header line starting with "parity" */
            if (name == "parity"){
                break; /* break the loop when header line found */
            }
        }
        N=0;
        /* get N+1 lines of state */
        while(std::getline(file,line)) {
            std::stringstream line_stream(line);
            N = N+1;
            /* first number is vertex id */
            size_t id;
            line_stream >> id;
            /* second number is color of that vertex */
            size_t color;
            line_stream >> color;
            /* third number is owner of that vertex */
            size_t owner_id;
            line_stream >> owner_id;
            /* set the color and owner of that vertex */
            V_name.push_back(id);
            V_ID.push_back(owner_id);
            COL.push_back(color);
            /* update number of color if color of this vertex is more than N_COL */
            if (color >= N_COL)
                N_COL = color + 1;
            // std::cout <<"\n num=" << N << " v_id = " << id << " owner=" << owner_id << " color=" << color << "\n";
            std::string successors; /* string of successors of that vertex */
            line_stream >> successors;
            std::stringstream ss(successors);
            std::unordered_set<size_t> trans_list;
            /* update transitions of that vertex */
            for (size_t i; ss >> i; ++ i) {
                trans_list.insert(i);   
                // std::cout << i << ",";
                if (ss.peek() == ',')
                    ss.ignore();
            }
            TR.push_back(trans_list);
        }
        file.close();
        for (auto it = TR.begin(); it != TR.end(); ++ it){
            std::unordered_set<size_t> trans_list;
            std::unordered_set<size_t> set = *it;
            for (auto i : set){
                trans_list.insert(std::distance(V_name.begin(), find(V_name.begin(),V_name.end(),i)));
            }
            TR_final.push_back(trans_list);
        }
        return 1;
    } else {
        try {
            throw std::runtime_error("FileHandler:pg2game: Unable to open input file.");
        } catch (std::exception &e) {
            std::cout << e.what() << "\n";
            return 0;
        }
    }
}

/* Check if input is a .hoa file, then we use the hoa game constructor */
inline bool ends_with(std::string const & value, std::string const & ending)
{
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

/* Print out the set of assumptions */
void print_unsafe_edges(std::multimap<size_t, size_t> unsafe_edges){
    std::cout << "\n Set of unsafe edges:\n";
    for (auto e = unsafe_edges.begin(); e != unsafe_edges.end(); ++e) {
        std::cout << e->first << " -> " << e->second << "\n";
    }
}
void print_depressed_edges(std::multimap<size_t, size_t> depressed_edge_set){
    std::cout << "\n Set of depressed edges:\n";
    for (auto e = depressed_edge_set.begin(); e != depressed_edge_set.end(); ++e) {
        std::cout << e->first << " -> " << e->second << "\n";
    }
}
void print_live_groups(std::vector<std::multimap<size_t, size_t>> live_group_set){
    std::cout << "\n set of live groups:\n";
    for (auto lg = live_group_set.begin(); lg != live_group_set.end(); ++lg) {
        std::cout << "{";
        for (auto m = lg->begin(); m != lg->end();) {
            std::cout << "(" << m->first << " -> " << m->second << ")";
            ++m;
            if (m != lg->end())
                std::cout << ", ";
        }
        std::cout << "}\n";
    }
}
void print_conditional_live_groups(std::vector<std::unordered_set<size_t>> condition_sets,
                                    std::vector<std::vector<std::multimap<size_t, size_t>>> conditional_live_group_set){
    std::cout << "\n set of conditional live groups:\n";
    size_t count_condition = 0;
    
    for (auto g = conditional_live_group_set.begin(); g != conditional_live_group_set.end(); ++g){
        for (auto lg = g->begin(); lg != g->end(); ++lg) {
            std::cout <<"{";
            std::function<std::string()> f = [&]() {f = [](){ return ","; }; return ""; };
            for (auto const &i: condition_sets[count_condition]) {
                std::cout << f() << i;
            }
            std::cout << "}:{";
            for (auto m = lg->begin(); m != lg->end();) {
                std::cout << "(" << m->first << " -> " << m->second << ")";
                ++m;
                if (m != lg->end())
                    std::cout << ", ";
            }
            std::cout << "}\n";
        }
        count_condition += 1;
    }            
}
void print_live_edges(std::multimap<size_t, size_t> live_edges){
    std::cout << "\n Set of live edges:\n";
    for (auto e = live_edges.begin(); e != live_edges.end(); ++e) {
        std::cout << e->first << " -> " << e->second << "\n";
    }
}
