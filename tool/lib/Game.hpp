/*
 * Class: Game
 *
 *  Class formalizing a basic two player parity game
 */

#ifndef GAME_HPP_
#define GAME_HPP_

#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_set>
#include <map>
#include <queue>

// #include "FileHandler.hpp"
// #include <utility>

#define V0 0 /* vertices belonging to Player 0 */
#define V1 1 /* vertices belonging to Player 1 */

namespace mpa {

struct strategy {
    std::unordered_set<size_t> domain; /* the domain of the strategy (winning domain, when the strategy is a winning strategy) */
    std::map<size_t, size_t> action; /* player 0's strategy (player 1's states which are in domain do not get any action assigned) */
    void clear() {
        domain.clear();
        action.clear();
    }
};

class Game {
public:
    /* number of vertices */
    size_t n_vert_;
    /* vertex id: V0, when the vertex belongs to player 0 and V1 when it belongs to player 1 */
    std::vector<size_t> vert_id_;
    /* transitions: element i represents the set of successors of vertex i */
    std::vector<std::unordered_set<size_t>> tr_;
    /* inverse of the transition relation */
    std::vector<std::unordered_set<size_t>> pre_tr_;
public:
    /* default constructor */
    Game() {
        n_vert_ = 0;
    }
    /* basic constructor from given explicit representation of the game graph */
    Game(size_t n_vert,
         std::vector<size_t> vert_id,
         std::vector<std::unordered_set<size_t>> tr): n_vert_(n_vert), vert_id_(vert_id), tr_(tr) {
        /* sanity check for vertex id-s */
        for (auto i = vert_id_.begin(); i != vert_id_.end(); ++i) {
            if ((*i != V0) && (*i != V1)) {
                std::cerr << "[WARNING] Invalid assignment of vertices to Player 0 and Player 1.\n";
            }
        }
        /* populate pre_tr_ */
        for (size_t i = 0; i < n_vert_; i++) {
            std::unordered_set<size_t> s;
            pre_tr_.push_back(s);
        }
        for (size_t u = 0; u < n_vert_; u++) {
            for (auto v = tr_[u].begin(); v != tr_[u].end(); ++v) {
                pre_tr_[*v].insert(u);
            }
        }
    }


    ///////////////////////////////////////////////////////////////
    ///Buchi games
    ///////////////////////////////////////////////////////////////

    /* compute the assumptions that will ensure that the sure and maybe winning regions are the same */
    void find_assumption_buchi(const std::unordered_set<size_t> target,
                                std::multimap<size_t, size_t>& unsafe_edges,
                                std::vector<std::multimap<size_t, size_t>>& live_group_set) const {
        /* first compute the maybe winning region and the set of unsafe edges */
        std::unordered_set<size_t> maybe_winning_region;
        find_unsafe_edges_buchi(target, maybe_winning_region, unsafe_edges);
        /* surely losing region is the complement of maybe winning region*/
        std::unordered_set<size_t> surely_losing_region = set_complement(maybe_winning_region);
        /* copy the current game */
        Game game_copy(*this);
        /* remove all the edges with one end in surely_losing_region */
        game_copy.remove_vertices(surely_losing_region);
        game_copy.find_live_groups_buchi(target, maybe_winning_region, live_group_set);
    }

    /* compute the set of unsafe edges */
    void find_unsafe_edges_buchi(const std::unordered_set<size_t> target,
                                    std::unordered_set<size_t>& maybe_winning_region,
                                    std::multimap<size_t, size_t>& unsafe_edges) const {
        /* find the maybe winning region */
        strategy C_coop = solve_coop_buchi_game(target);
        maybe_winning_region = C_coop.domain;
        /* the surely losing region is the complement of maybe winning region */
        std::unordered_set<size_t> surely_losing_region = set_complement(maybe_winning_region);
        /* every player 1 edge starting in the maybe winning region and ending in the surely losing region is an unsafe edge */
        for (auto u = maybe_winning_region.begin(); u != maybe_winning_region.end(); ++u) {
            if (vert_id_[*u] == V1) { /* u is a player 1 vertex */
                std::unordered_set<size_t> unsafe_successors = set_intersection(tr_[*u], surely_losing_region); /* set of successors of u in the surely losing region */
                for (auto v = unsafe_successors.begin(); v != unsafe_successors.end(); ++v) {
                    unsafe_edges.insert(std::pair<size_t, size_t>(*u, *v));
                }
            }
        }
    }

    /* compute the set of live groups */
    void find_live_groups_buchi(const std::unordered_set<size_t> target,
                                const std::unordered_set<size_t> maybe_winning_region,
                                std::vector<std::multimap<size_t, size_t>>& live_group_set) const {
        std::unordered_set<size_t> curr_winning_region = set_intersection(maybe_winning_region, target);
        strategy C;
        /* keep finding live groups until convergence to the maybe winning region */
        while (1) {
            C.clear();
            solve_reachability_game(curr_winning_region, C);
            curr_winning_region = C.domain;
            if (curr_winning_region == maybe_winning_region) {
                break;
            }
            std::multimap<size_t, size_t> live_group = find_live_group_one_step(curr_winning_region);
            live_group_set.push_back(live_group);
            for (auto m = live_group.begin(); m != live_group.end(); ++m) {
                curr_winning_region.insert(m->first);
            }
        }
    }

    /* find a single live group that can extend the given winning domain
     * a live group is a multimap v|->v' such that
     *  - v is a player 1 vertex outside winning_domain
     *  - v' is in winning_domain
     *  - (v,v') is a transition */
    std::multimap<size_t, size_t> find_live_group_one_step(const std::unordered_set<size_t> winning_domain) const {
        /* sanity check: winning_domain cannot be extended by player 0 alone */
        bool sanity_check_failed = false;
        /* iterate over every vertex in the winning domain */
        for (auto v = winning_domain.begin(); v != winning_domain.end(); ++v) {
            /* iterate over every predecessors */
            for (auto u = pre_tr_[*v].begin(); u != pre_tr_[*v].end(); ++u) {
                if (winning_domain.find(*u)==winning_domain.end()) { /* the predecessor is outside the winning domain */
                    if (vert_id_[*u] == V0) { /* player 0 vertex */
                        sanity_check_failed = true;
                        break;
                    } else { /* player 1 vertex */
                        if (check_set_inclusion(tr_[*u], winning_domain)) { /* every successor of u is in the winning domain, so u should've been winning */
                            sanity_check_failed = true;
                            break;
                        }
                    }
                }
            }
            if (sanity_check_failed) {
                std::cerr << "[WARNING] Game::find_live_group: The given winning_domain can be further expanded by Player 0. The computed live group may contain spurious edges.\n";
                break;
            }
        }
        /* construct the live group*/
        std::multimap<size_t, size_t> live_group;
        /* iterate over every vertex in the winning domain */
        for (auto v = winning_domain.begin(); v != winning_domain.end(); ++v) {
            /* iterate over every predecessors */

            for (auto u = pre_tr_[*v].begin(); u != pre_tr_[*v].end(); ++u) {
                // std::cout << "new edge:" << *u << *v << "\n";
                if (winning_domain.find(*u) == winning_domain.end()) { /* u is not in winning domain */
                    /* if the sanity check was successful, then
                     * u is a player 1 vertex with some successors going outside of the winning domain
                     * (u,v) must be a live edge */
                    std::pair<size_t, size_t> live_edge(*u, *v);
                    live_group.insert(live_edge);
                }
            }
        }
        return live_group;
    }

    /* solve reachability game
     * input: target
     * output: strategy */
    void solve_reachability_game(const std::unordered_set<size_t> target, strategy& C) const {
        /* used to encode that a state is not in the winning domain */
        size_t losing = std::numeric_limits<size_t>::max();
        /* the value/rank of a vertex; the value of losing vertices = losing */
        std::vector<size_t> value(n_vert_,losing);
        /* intermediate values of vertices */
        std::vector<size_t> interim_value(n_vert_,0);
        /* a list of processed vertices */
        std::unordered_set<size_t> processed;
        /* the fifo queue that keeps track of the winning region */
        std::queue<size_t> fifo;
        /* initialize the data structures with the target */
        for (auto t = target.begin(); t != target.end(); ++t) {
            value[*t] = 0;
            C.domain.insert(*t);
            if (vert_id_[*t] == V0) { /* player 0 vertex, action assignment is needed */
                C.action.insert(std::pair<size_t, size_t>(*t, *(tr_[*t].begin())));
            }
            fifo.push(*t);
        }
        /* the main while loop */
        while (fifo.size() != 0) {
            /* get the oldest element from fifo */
            size_t v = fifo.front();
            fifo.pop();
            /* bookkeeping */
            processed.insert(v);
            /* iterate over all the predecessors of u */
            for (auto u = pre_tr_[v].begin(); u != pre_tr_[v].end(); ++u) {
                interim_value[*u] = std::max(interim_value[*u], value[v]);
                if (value[*u] > 1 + interim_value[*u]) {
                    if (vert_id_[*u] == V0) { /* u is player 0 vertex */
                        /* update the value of u */
                        value[*u] = 1 + value[v];
                        fifo.push(*u);
                        C.domain.insert(*u);
                        C.action.insert(std::pair<size_t, size_t>(*u, v));
                    } else { /* u is player 1 vertex */
                        /* only update the value of u when all the successors of u are processed */
                        bool this_vertex_is_winning = true;
                        for (auto w = tr_[*u].begin(); w != tr_[*u].end(); ++w) {
                            if (processed.find(*w) == processed.end()) {
                                this_vertex_is_winning = false;
                                break;
                            }
                        }
                        if (this_vertex_is_winning) {
                            value[*u] = 1 + value[v];
                            fifo.push(*u);
                            C.domain.insert(*u);
                        }
                    }
                }
            }
        }
    }

    /* solve Buchi game */
    strategy solve_buchi_game(const std::unordered_set<size_t> target) const {
        strategy C;
        /* the nu fixpoing variable */
        std::unordered_set<size_t> winning_domain_over_approx;
        /* initially every vertex is assumed to be winning */
        for (size_t i = 0; i < n_vert_; i++) {
            winning_domain_over_approx.insert(i);
        }
        /* outer fixpoint */
        while (1) {
            C.domain.clear();
            C.action.clear();
            /* iterate over every vertex in the Buchi target set
             * and only keep those from which visit to
             * winning_domain_over_approx can be enforced in the following step */
            for (auto u = target.begin(); u != target.end(); ++u) {
                if (vert_id_[*u] == V0) { /* player 0 vertex */
                    size_t w;
                    if (check_set_intersection(tr_[*u], winning_domain_over_approx, w)) {
                        C.domain.insert(*u);
                        C.action.insert(std::pair<size_t, size_t>(*u, w));
                    }
                } else { /* player 1 vertex */
                    if (check_set_inclusion(tr_[*u], winning_domain_over_approx)) {
                        C.domain.insert(*u);
                    }
                }
            }
            strategy C_partial; /* the strategy for the reachability part */
            solve_reachability_game(C.domain, C_partial);
            /* check convergence of the outer fixpoint */
            if (winning_domain_over_approx == C_partial.domain) { /* converged */
                /* merge the strategies for the reachability and the safety parts */
                for (auto u = C_partial.domain.begin(); u != C_partial.domain.end(); ++u) {
                    if (C.domain.find(*u) != C.domain.end()) { /* u is already in C.domain, meaning u must have come from the safety part: ignore */
                        continue;
                    }
                    C.domain.insert(*u);
                    C.action.insert(std::pair<size_t, size_t>(*u, C_partial.action[*u]));
                }
                break;
            } else
                winning_domain_over_approx = C_partial.domain;
        }
        return C;
    }

    /* solve cooperative Buchi game */
    strategy solve_coop_buchi_game(const std::unordered_set<size_t> target) const {
        /* make a copy of the game and make every vertex player 0 vertex */
        Game game_copy = *this;
        game_copy.vert_id_ = std::vector<size_t>(n_vert_, V0);
        /* solve the normal buchi game on this copy */
        return game_copy.solve_buchi_game(target);
    }

    ///////////////////////////////////////////////////////////////
    ///Co-Buchi games
    ///////////////////////////////////////////////////////////////

    /* compute the assumptions that will ensure that the sure and maybe winning regions are the same */
    void find_assumption_cobuchi(const std::unordered_set<size_t> target,
                                    std::multimap<size_t, size_t>& unsafe_edges,
                                    std::multimap<size_t, size_t>& depressed_edge_set) const {
        /* first compute the maybe winning region and the set of unsafe edges */
        std::unordered_set<size_t> maybe_winning_region;
        find_unsafe_edges_cobuchi(target, maybe_winning_region, unsafe_edges);
        /* surely losing region is the complement of maybe winning region*/
        std::unordered_set<size_t> surely_losing_region = set_complement(maybe_winning_region);
        /* copy the current game */
        Game game_copy(*this);
        /* remove all the edges with one end in surely_losing_region */
        game_copy.remove_vertices(surely_losing_region);
        /* finally compute the live groups and depressed edges */
        game_copy.find_depressed_edge_set_cobuchi(target, maybe_winning_region, depressed_edge_set);
    }

    /* compute the set of unsafe edges */
    void find_unsafe_edges_cobuchi(const std::unordered_set<size_t> target,
                                    std::unordered_set<size_t>& maybe_winning_region,
                                    std::multimap<size_t, size_t>& unsafe_edges) const {
        /* find the maybe winning region */
        maybe_winning_region = solve_coop_cobuchi_game(target);
        /* the surely losing region is the complement of maybe winning region */
        std::unordered_set<size_t> surely_losing_region = set_complement(maybe_winning_region);
        /* every player 1 edge starting in the maybe winning region and ending in the surely losing region is an unsafe edge */
        for (auto u = maybe_winning_region.begin(); u != maybe_winning_region.end(); ++u) {
            if (vert_id_[*u] == V1) { /* u is a player 1 vertex */
                std::unordered_set<size_t> unsafe_successors = set_intersection(tr_[*u], surely_losing_region); /* set of successors of u in the surely losing region */
                for (auto v = unsafe_successors.begin(); v != unsafe_successors.end(); ++v) {
                    unsafe_edges.insert(std::pair<size_t, size_t>(*u, *v));
                }
            }
        }
    }

    /* compute the set of depressed edges */
    void find_depressed_edge_set_cobuchi(const std::unordered_set<size_t> target,
                                        const std::unordered_set<size_t> maybe_winning_region,
                                        std::multimap<size_t, size_t>& depressed_edge_set) const {
        std::unordered_set<size_t> curr_winning_region = solve_coop_safety_game(target);
        // print_set("curr_winning_region 0:",curr_winning_region);
        // print_set("maybe win:", maybe_winning_region);
        std::unordered_set<size_t> curr_losing_region = set_difference(maybe_winning_region, curr_winning_region);
        /* first set of depressed edges are the edges from safety winning region to its complement*/
        for(auto a = curr_winning_region.begin(); a!= curr_winning_region.end(); ++a){
            if (vert_id_[*a] == V1){
                for (auto b = tr_[*a].begin(); b != tr_[*a].end(); ++b){
                    if (curr_losing_region.find(*b) != curr_losing_region.end()) {
                        depressed_edge_set.insert(std::pair<size_t, size_t>(*a, *b));
                    }
                }
            }
        }
        std::unordered_set<size_t> pre_winning;
        strategy C;
        /* keep finding depressed until convergence to the maybe winning region */
        while (1){
            C.clear();
            solve_reachability_game(curr_winning_region, C);
            curr_winning_region = C.domain;
            if (curr_winning_region == maybe_winning_region) {
                break;
            }
            find_depressed_edges_one_step(curr_winning_region, depressed_edge_set);
        }
    }

    /* find a single live group that can extend the given winning domain
     * a live group is a multimap v|->v' such that
     *  - v is a player 1 vertex outside winning_domain
     *  - v' is in winning_domain
     *  - (v,v') is a transition */
    void find_depressed_edges_one_step(std::unordered_set<size_t>& winning_domain,
                                        std::multimap<size_t, size_t>& depressed_edge_set) const {
        /* sanity check: winning_domain cannot be extended by player 0 alone */
        bool sanity_check_failed = false;
        /* iterate over every vertex in the winning domain */
        for (auto v = winning_domain.begin(); v != winning_domain.end(); ++v) {
            /* iterate over every predecessors */
            for (auto u = pre_tr_[*v].begin(); u != pre_tr_[*v].end(); ++u) {
                if (winning_domain.find(*u)==winning_domain.end()) { /* the predecessor is outside the winning domain */
                    if (vert_id_[*u] == V0) { /* player 0 vertex */
                        sanity_check_failed = true;
                        break;
                    } else { /* player 1 vertex */
                        if (check_set_inclusion(tr_[*u], winning_domain)) { /* every successor of u is in the winning domain, so u should've been winning */
                            sanity_check_failed = true;
                            break;
                        }
                    }
                }
            }
            if (sanity_check_failed) {
                std::cerr << "[WARNING] Game::find_live_group: The given winning_domain can be further expanded by Player 0. The computed live group may contain spurious edges.\n";
                break;
            }
        }
        /* compute the depressed edges */
        /* iterate over every vertex in the winning domain */
        for (auto v = winning_domain.begin(); v != winning_domain.end(); ++v) {
            /* iterate over every predecessors */

            for (auto u = pre_tr_[*v].begin(); u != pre_tr_[*v].end(); ++u) {
                // std::cout << "new edge:" << *u << *v << "\n";
                if (winning_domain.find(*u) == winning_domain.end()) { /* u is not in winning domain */
                    /* if the sanity check was successful, then
                     * u is a player 1 vertex with with some edges going outside of the winning domain,
                     which should be depressed */
                    for (auto w = tr_[*u].begin(); w != tr_[*u].end(); ++w){
                        if (winning_domain.find(*w) == winning_domain.end()) {
                            /* (u,w) goes out of winning domain, hence included in depressed edges */
                            depressed_edge_set.insert(std::pair<size_t, size_t> (*u, *w));
                        }
                    }
                    winning_domain.insert(*u);
                }
            }
        }
    }

    /* solve cooperative safety game
     * input: target
     * output: winning region */
    std::unordered_set<size_t> solve_coop_safety_game(const std::unordered_set<size_t> target) const {
        /* initialize current winning region = target */
        std::unordered_set<size_t> curr_winning_region = target;
        /* all the vertices in target from which there is an edge to target */
        std::unordered_set<size_t> pre_winning = set_intersection(compute_pre_set(curr_winning_region), target);
        /* keep iterating until convergence to the maybe winning region */
        while (curr_winning_region != pre_winning){
            /* include all vertices in pre(winning region) \cap targets in winning region */
            curr_winning_region.insert(pre_winning.begin(), pre_winning.end());
            /* all the vertices in target from which there is an edge to current winning region */
            pre_winning = set_intersection(compute_pre_set(curr_winning_region), target);
        }
        return curr_winning_region;
    }

    /* solve cooperative co-Buchi game
     * input: target
     * output: winning region */
    std::unordered_set<size_t> solve_coop_cobuchi_game(const std::unordered_set<size_t> target) const {
        /* initialize current winning region = saftey winning region */
        std::unordered_set<size_t> curr_winning_region = solve_coop_safety_game(target);
        /* pre of current winning region */
        std::unordered_set<size_t> pre_winning = compute_pre_set(curr_winning_region);
        /* keep iterating until convergence to the maybe winning region */
        while (curr_winning_region != pre_winning){
            /* include all vertices in pre(winning region) in winning region */
            curr_winning_region.insert(pre_winning.begin(), pre_winning.end());
            /* pre of current winning region */
            pre_winning = compute_pre_set(curr_winning_region);
        }
        return curr_winning_region;
    }
public:
    ///////////////////////////////////////////////////////////////
    ///Basic functions
    ///////////////////////////////////////////////////////////////

    /* function: compute_pre_set
    *
    compute predecessors of verictes in a set */
    std::unordered_set<size_t> compute_pre_set(const std::unordered_set<size_t> set1) const {
        std::unordered_set<size_t> pre_set;
        for (auto u = set1.begin(); u != set1.end(); u++) {
            for (auto v = pre_tr_[*u].begin(); v != pre_tr_[*u].end(); ++v){
                pre_set.insert(*v);
            }
        }
        return pre_set;
    }

    /* function: compute_cpre_set
    *
    compute controllable predecessors of verictes in a set */
    std::unordered_set<size_t> compute_cpre_set(const std::unordered_set<size_t> set1) const {
        std::unordered_set<size_t> cpre_set;
        for (auto v = set1.begin(); v != set1.end(); v++) {
            for (auto u = pre_tr_[*v].begin(); u != pre_tr_[*v].end(); ++u){
                if (vert_id_[*u] == V0) {
                    cpre_set.insert(*u);
                } else {
                    if (check_set_inclusion(tr_[*u], set1)) {
                        cpre_set.insert(*u);
                    }
                }
            }
        }
        return cpre_set;
    }

    /* function: check_set_inclusion
     *
     * check if set1 is included in set2 */
    template<class T>
    bool check_set_inclusion(const std::unordered_set<T> set1, const std::unordered_set<T> set2) const {
        for (auto a = set1.begin(); a != set1.end(); ++a) {
            if (set2.find(*a) == set2.end()) {
                return false;
            }
        }
        return true;
    }

    /* function: check_set_intersection
     *
     * check if there is nonempty intersection between the two sets */
    template<class T>
    bool check_set_intersection(const std::unordered_set<T> set1, const std::unordered_set<T> set2, T& witness=T()) const {
        for (auto a = set1.begin(); a != set1.end(); ++a) {
            if (set2.find(*a) != set2.end()) {
                witness = *a;
                return true;
            }
        }
        return false;
    }

    /* function: set_intersection
     *
     * compute intersection of two sets */
    template<class T>
    std::unordered_set<T> set_intersection(const std::unordered_set<T> set1, const std::unordered_set<T> set2) const {
        std::unordered_set<T> set3;
        for (auto a = set1.begin(); a != set1.end(); ++a) {
            if (set2.find(*a) != set2.end()) {
                set3.insert(*a);
            }
        }
        return set3;
    }

    /* function: set_union
     *
     * compute union of two sets */
    template<class T>
    std::unordered_set<T> set_union(const std::unordered_set<T> set1, const std::unordered_set<T> set2) const {
        std::unordered_set<T> set3;
        for (auto a = set1.begin(); a != set1.end(); ++a) {
            set3.insert(*a);
        }
        for (auto b = set2.begin(); b != set2.end(); ++b) {
            set3.insert(*b);
        }
        return set3;
    }

    /* function: set_difference
     *
     * compute set difference of two sets*/
    template<class T>
    std::unordered_set<T> set_difference(const std::unordered_set<T> set2, const std::unordered_set<T> set1) const {
        std::unordered_set<T> set3; /* set2 - set1 */
        for (auto a = set2.begin(); a != set2.end(); ++a) {
            if (set1.find(*a) == set1.end()) {
                set3.insert(*a);
            }
        }
        return set3;
    }

    /* function: set_complement
     *
     * compute complement of a set*/
    template<class T>
    std::unordered_set<T> set_complement(const std::unordered_set<T> set1) const {
        std::unordered_set<T> set3;
        for (size_t u = 0; u < n_vert_; u++) {
            if (set1.find(u) == set1.end()) {
                set3.insert(u);
            }
        }
        return set3;
    }

    /* function: remove_vertices
     *
     * remove all edges from/to a set of vertices*/
    template<class T>
    void remove_vertices(const std::unordered_set<T> set1) {
        for(auto a = set1.begin(); a!= set1.end(); ++a){
            tr_[*a].clear();
            pre_tr_[*a].clear();
        }
        for(size_t u = 0; u < n_vert_; ++u){
            tr_[u] = set_difference(tr_[u],set1);
            pre_tr_[u] = set_difference(pre_tr_[u],set1);
        }
    }

    /* function: print_set
     *
     * print out all elements of the set*/
    void print_set (const std::string note, const std::unordered_set<size_t> set) const {
        std::cout << "\n" << note << "::";
        for (auto u=set.begin(); u != set.end(); ++u){
            std::cout << *u << ",";
        }
        std::cout << "\n";
    }

}; /* close class definition */
} /* close namespace */

#endif
