/*
 * Class: ParityGame
 *
 * Class formalizing two player parity game
 */

#ifndef PARITYGAME_HPP_
#define PARITYGAME_HPP_

#include "Game.hpp"

#include "hoa_consumer_build_parity_game.hh"

namespace mpa {
class ParityGame: public Game {
public:
    /* number of colors */
    size_t n_colors_;
    /* colors: the i-th element represents the color of vertex i */
    std::vector<size_t> colors_;
public:
    /* basic constructor from given explicit representation of the game graph */
    ParityGame(size_t n_vert,
         std::vector<size_t> vert_id,
         std::vector<std::unordered_set<size_t>> tr,
         size_t n_colors,
         std::vector<size_t> colors): Game(n_vert, vert_id, tr), n_colors_(n_colors), colors_(colors) {}

    /* constructor: ParityGame
    * constructor from a HOA file */
    ParityGame(const std::string filename) {
        std::ifstream file(filename);
        cpphoafparser::HOAConsumer::ptr consumer;
        cpphoafparser::parity_game_data data;
        consumer.reset(new cpphoafparser::HOAConsumerBuildParityGame(&data));
        cpphoafparser::HOAParser::parse(file, consumer);
        n_vert_ = data.n_vert;
        vert_id_ = data.vert_id;
        tr_ = data.tr;
        pre_tr_ = data.pre_tr;
        n_colors_ = data.n_colors;
        colors_ = data.colors;
    }
    /* copy constructor */
    ParityGame(const ParityGame& other): Game(other) {
        n_vert_ = other.n_vert_;
        vert_id_ = other.vert_id_;
        tr_ = other.tr_;
        pre_tr_ = other.pre_tr_;
        n_colors_ = other.n_colors_;
        colors_ = other.colors_;
    }

    /* default constructor */
    ParityGame(): Game() {}
    ///////////////////////////////////////////////////////////////
    ///Parity games
    ///////////////////////////////////////////////////////////////

    /* compute the assumptions that will ensure that the sure and maybe winning regions are the same */
    void find_assumption_parity(std::multimap<size_t, size_t>& unsafe_edges,
            std::vector<std::unordered_set<size_t>>& condition_sets,
            std::vector<std::vector<std::multimap<size_t, size_t>>>& conditional_live_group_set,
            std::multimap<size_t, size_t>& depressed_edge_set) const {
        /* first compute the maybe winning region and the set of unsafe edges */
        std::unordered_set<size_t> maybe_winning_region;
        find_unsafe_edges_parity(maybe_winning_region, unsafe_edges);
        /* surely losing region is the complement of maybe winning region*/
        std::unordered_set<size_t> surely_losing_region = set_complement(maybe_winning_region);
        /* copy the current game */
        ParityGame game_copy(*this);
        /* remove all the edges with one end in surely_losing_region */
        game_copy.remove_vertices(surely_losing_region);
        /* finally compute the live groups and depressed edges */
        game_copy.find_live_depressed_assumption_parity(maybe_winning_region, condition_sets, conditional_live_group_set, depressed_edge_set);
    }

    /* compute the set of unsafe edges */
    void find_unsafe_edges_parity(std::unordered_set<size_t>& maybe_winning_region,
                                    std::multimap<size_t, size_t>& unsafe_edges) const {
        /* find the maybe winning region */
        maybe_winning_region = solve_coop_parity_game();
        /* the surely losing region is the complement of maybe winning region */
        std::unordered_set<size_t> surely_losing_region = set_complement(maybe_winning_region);
        /* every player 1 edge starting in the maybe winning region and ending in the surely losing region is an unsafe edge */
        for (auto u = maybe_winning_region.begin(); u != maybe_winning_region.end(); ++u) {
            if (vert_id_[*u] == V1) { /* u is a player 1 vertex */
                std::unordered_set<size_t> unsafe_successors = set_intersection(tr_[*u], surely_losing_region); /* set of successors of u in the surely losing region */
                for (auto v = unsafe_successors.begin(); v != unsafe_successors.end(); ++v) {
                    unsafe_edges.insert(std::pair<size_t, size_t>(*u, *v));
                }
            }
        }
    }

    /* compute the set of conditional live groups and depressed edges recursively */
    void find_live_depressed_assumption_parity(std::unordered_set<size_t>& maybe_winning_region,
                                                std::vector<std::unordered_set<size_t>>& condition_sets,
                                                std::vector<std::vector<std::multimap<size_t, size_t>>>& conditional_live_group_set,
                                                std::multimap<size_t, size_t>& depressed_edge_set) const {
        size_t max_col = max_color(maybe_winning_region); /* maximum vertex color in maybe winning region */
        // print_set("winning region", maybe_winning_region);
        // std::cout << "max_col:" << max_col << "\n";
        std::unordered_set<size_t> max_col_vertices; /* vertices with maximum color */
        ParityGame game_copy = *this; /* copy the game and use it for removing vertices */
        if (max_col%2 == 1){
            // std::cout << "odd \n";
            max_col_vertices = compute_color_equal(max_col, maybe_winning_region);
            // print_set("max_col_vertices", max_col_vertices);
            ParityGame game_new = *this; /* new game to solve parity without visiting max_col (odd) vertices */
            game_new.remove_vertices(max_col_vertices);
            /* vertices from which one can cooperatively win without visiting max_col_vertices */
            std::unordered_set<size_t> cobuchi_winning_region = game_new.solve_coop_parity_game();
            // print_set("cobuchi winning region",cobuchi_winning_region);
            // print_set("maybe winning region",maybe_winning_region);
            /* a winning play eventually stays in the cobuchi_winning_region
            as it does not visit (odd) max_color vertices infinitely often*/
            game_copy.find_depressed_edge_set_cobuchi(cobuchi_winning_region, maybe_winning_region, depressed_edge_set);
            std::unordered_set<size_t> cobuchi_losing_region = set_difference(maybe_winning_region, cobuchi_winning_region);
            /* depressed edges ensures that a play eventually stays in cobuchi_winning_region,
            now only need to deal with that region */
            game_copy.remove_vertices(cobuchi_losing_region);
            game_copy.find_live_depressed_assumption_parity(cobuchi_winning_region, condition_sets, conditional_live_group_set, depressed_edge_set);
        } else {
            max_col_vertices = compute_color_equal(max_col, maybe_winning_region);
            /* vertices in maybe_winning_region from which one can cooperatively
            visit (even) max_color_vertices infinitely often */
            std::unordered_set<size_t> buchi_winning_region = solve_coop_buchi_game(max_col_vertices).domain;
            std::unordered_set<size_t> buchi_losing_region = set_difference(maybe_winning_region, buchi_winning_region);
            if (!buchi_winning_region.empty()){
                // std::cout << "buchi_winning \n";
                // print_set("buchi_winning_vertices", buchi_winning_region);
                // print_set("max_col_vertices", max_col_vertices);
                ParityGame game_new = *this;
                /* if buchi_winning_region is not empty, first we deal with that region */
                game_new.remove_vertices(buchi_losing_region);
                /* iterate over every odd color from 1 to max_col-1 */
                for (size_t col = 1; col < max_col; ){
                    /* vertices with color = col */
                    std:: unordered_set<size_t> col_vertices = compute_color_equal(col, buchi_winning_region);
                    // print_set("col_vertices", col_vertices);
                    /* if col_vertices is non-empty, first compute vertices with even color
                    that is greater than col and solve buchi with them as target */
                    if (col_vertices.empty() == false){
                        /* vertices with even color that is greater than col */
                        std::unordered_set<size_t> col_target;
                        /* iterate over every even color from col to max_col */
                        for (size_t win_col = col+1; win_col <= max_col; ) {
                            std:: unordered_set<size_t> win_col_vertices = compute_color_equal(win_col, buchi_winning_region);
                            // std::cout << win_col;
                            // print_set("win_col_vert", win_col_vertices);
                            col_target.insert(win_col_vertices.begin(), win_col_vertices.end());
                            win_col += 2;
                        }
                        // print_set("col_target", col_target);
                        std::vector<std::multimap<size_t, size_t>> col_live_group_set;
                        /* if a play visit col_vertices infinitely often,
                        it should visit col_target (vertices with even and > col color) infinitely often*/
                        game_new.find_live_groups_buchi(col_target, buchi_winning_region, col_live_group_set);
                        if (!col_live_group_set.empty()){
                            condition_sets.push_back(col_vertices);
                            conditional_live_group_set.push_back(col_live_group_set);
                        }
                    }
                    col += 2; /* only consider odd colors col in the loop */
                }
            }
            if (!buchi_losing_region.empty()){
                /* if buchi_losing_region is non-empty,
                 finally we only need to deal with them */
                // std::cout << "buchi_losing \n";
                game_copy.remove_vertices(buchi_winning_region);
                max_col_vertices = compute_color_equal(max_col, buchi_losing_region);
                /* a play can not visit (even) max_col infinitely often,
                so we change their color to 0 and solve odd degree color parity game */
                if (!max_col_vertices.empty()){
                    for (auto u = max_col_vertices.begin(); u != max_col_vertices.end(); ++u){
                        game_copy.colors_[*u] = 0;
                    }
                }
                /* there is no unsafe edges,
                so we start with find_live_depressed_assumption directly */
                game_copy.find_live_depressed_assumption_parity(buchi_losing_region, condition_sets, conditional_live_group_set, depressed_edge_set);
            }
        }
    }

    /* solve parity game */
    std::unordered_set<size_t> solve_parity_game() const {
        std::unordered_set<size_t> set; /* winning set on which we use while condition*/
        std::unordered_set<size_t> even_vertex; /* vertices with even color */
        std::unordered_set<size_t> odd_vertex; /* vertices with odd color */
        /* compute odd and even colored vertices */
        for (size_t u = 0; u< n_vert_; ++u){
            if (colors_[u]%2 == 0){
                even_vertex.insert(u);
            } else {
                odd_vertex.insert(u);
            }
        }
        /* start with set = even colored vertices */
        set = even_vertex;
        /* continue until set = cpre(set) */
        while (set != compute_cpre_set(set))
        {
            parity_next(set, odd_vertex, even_vertex);
        }
        return set;
    }

    /* solve cooperative parity game */
    std::unordered_set<size_t> solve_coop_parity_game() const {
        /* make a copy of the game and make every vertex player 0 vertex */
        ParityGame game_copy = *this;
        game_copy.vert_id_ = std::vector<size_t>(n_vert_, V0);
        /* solve the normal buchi game on this copy */
        return game_copy.solve_parity_game();
    }

    /* compute next(S) using Bruse et al. */
    void parity_next(std::unordered_set<size_t>& set,
                        const std::unordered_set<size_t> odd_vertex,
                        const std::unordered_set<size_t> even_vertex) const {
        std::unordered_set<size_t> set_phi = compute_cpre_set(set); /* cpre(set) */
        size_t i = n_colors_; /* initialize i = largest color */
        /* compute min color in symmetric difference of set and set_phi = pre(set) */
        for (size_t u = 0; u < n_vert_; ++u){
            if (set.find(u) != set.end()){ /* if u belongs to set */
                if (set_phi.find(u) == set_phi.end()){ /* if u belongs to set\set_phi */
                    if (colors_[u] < i){
                        i = colors_[u]; /* if color of u < i then set i = color(u) */
                    }
                }
            /* if u belongs to set_phi\set */
            } else if (set_phi.find(u) != set_phi.end()){
                    if (colors_[u] < i){
                        i = colors_[u]; /* if color of u < i then set i = color(u) */
                    }
                }
        }
        std::unordered_set<size_t> set_R;
        if (i%2 == 0){
            /* if i is even, set_R is set\odd_vertex*/
            set_R = set_difference(set, odd_vertex);
        } else {
            /* if i is odd, set_R is set U even_vertex*/
            set_R = set_union(set, even_vertex);
        }
        std::unordered_set<size_t> set1 = compute_color_more(i,set); /* vertices in set with color > i */
        std::unordered_set<size_t> set2 = compute_color_equal(i,set_phi); /* vertices in set_phi with color = i */
        std::unordered_set<size_t> set3 = compute_color_less(i,set_R); /* vertices in set_R with color < i */
        std::unordered_set<size_t> set4 = set_union(set1,set2);
        set = set_union(set3, set4); /* set = union of set1, set2, set3 */
    }
public:
    /* function: max_color
    *
    compute maximum color in a set*/
    size_t max_color(const std::unordered_set<size_t> set) const {
        size_t i = 0;
        for (auto u = set.begin(); u != set.end(); ++u){
            if (colors_[*u] > i){
                i = colors_[*u];
            }
        }
        return i;
    }

    /* function: compute_color_i
    *
    compute verictes of color ?i */
    std::unordered_set<size_t> compute_color_less(const size_t i, const std::unordered_set<size_t> set1) const {
        std::unordered_set<size_t> set2;
        for (auto u = set1.begin(); u != set1.end(); ++u) {
            if (colors_[*u]<i) {
                set2.insert(*u);
            }
        }
        return set2;
    }
    std::unordered_set<size_t> compute_color_more(const size_t i, const std::unordered_set<size_t> set1) const {
        std::unordered_set<size_t> set2;
        for (auto u = set1.begin(); u != set1.end(); ++u) {
            if (colors_[*u]>i) {
                set2.insert(*u);
            }
        }
        return set2;
    }
    std::unordered_set<size_t> compute_color_equal(const size_t i, const std::unordered_set<size_t> set1) const {
        std::unordered_set<size_t> set2;
        for (auto u = set1.begin(); u != set1.end(); ++u) {
            if (colors_[*u]==i) {
                set2.insert(*u);
            }
        }
        return set2;
    }

    /* remove a set of outgoing edges from a vertex */
    void remove_outgoing_edges(size_t curr_vert,
                                std::unordered_set<size_t> succ_to_be_removed) {
        for (auto v = succ_to_be_removed.begin(); v != succ_to_be_removed.end(); ++v) {
            if (tr_[curr_vert].find(*v) == tr_[curr_vert].end()) {
                std::runtime_error("ParityGame::remove_outgoing_edges: some edge, which is to be removed, is invalid.");
            } else {
                tr_[curr_vert].erase(*v);
                pre_tr_[*v].erase(curr_vert);
            }
        }
    }

    /* append a vertex */
    void append_vertex(size_t id,
                       std::unordered_set<size_t> pred,
                       std::unordered_set<size_t> succ,
                       size_t color) {
        /* id of the new vertex */
        vert_id_.push_back(id);
        /* the transitions from vertices in pred to the added vertex */
        for (auto u = pred.begin(); u != pred.end(); ++u) {
            tr_[*u].insert(n_vert_);
        }
        pre_tr_.push_back(pred);
        /* the transitions from the added vertex to the vertices in succ */
        tr_.push_back(succ);
        for (auto v = succ.begin(); v != succ.end(); ++v) {
            pre_tr_[*v].insert(n_vert_);
        }
        /* color of the newly added vertex */
        colors_.push_back(color);
        if (color > n_colors_) {
            n_colors_ = color;
        }
        /* update the number of vertices */
        n_vert_++;
    }
}; /* close class definition */
} /* close namespace */

#endif
