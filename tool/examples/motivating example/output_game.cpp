#include <bitset>
#include <iostream>
#include <sstream>
#include <fstream>
#include <locale>
#include <vector>
#include <unordered_set>
#include <dirent.h>
#include <sys/stat.h>
#include <cstring>

int getIndex(std::vector<std::string> v, std::string K)
{
    std::vector<std::string>::iterator it;
    it = std::find(v.begin(), v.end(), K);
  
    // If element was found
    if (it != v.end()) 
    {
      
        // calculating the index
        // of K
        int index = it - v.begin();
        return index;
    }
    else {
        // If the element is not
        // present in the vector
        return 512;
    }
}

std::string binary(int num){
    std::string sa = std::bitset< 9 >( num ).to_string(); // string conversion
    return sa;
}

int main(){
    std::vector<std::string> state_list;

    std::string in_state = binary(0);
    state_list.push_back(in_state);
    int current_state = 0;

    while (current_state < 512)
    {
        std::string state = state_list[current_state];
        std::cout << current_state;
        if (state.substr(5,4) == "0000"){
            std::cout << " " << 2;
        }
        else{
            std::cout << " " << 1;
        }
        std::cout << " " << state[0] << " ";

        std::string neighbour_list;
        for (int i=0; i<512; ++i){
            std::string neighbour = binary(i);
            if (state[0] == '1'){
                if (neighbour[0] != '0')
                    continue;
                if (neighbour.substr(3,6) != state.substr(3,6))
                    continue;
            }
            if (state[0] == '0'){
                if (neighbour[0] != '1')
                    continue;
                if (neighbour.substr(1,2) != state.substr(1,2))
                    continue;
                if (neighbour.substr(3,2) == "11")
                    continue;
                
                if (state[1]=='1' && state[3]=='1'){
                    if(neighbour[3]!='1' || neighbour[5]!='0' || neighbour[7]!=state[7])
                        continue;
                }
                if (state[1]=='0' && state[3]=='0'){
                    if(neighbour[3]!='0' || neighbour[5]!=state[5] || neighbour[7]!='0')
                        continue;
                }
                if (state[1]=='1' && state[3]=='0'){
                    if (neighbour[3]=='1'){
                        if (neighbour[5]!='0' || neighbour[7]!='0')
                            continue;
                    }
                    if (neighbour[3]=='0'){
                        if (neighbour[5]!='1' || neighbour[7]!='0')
                            continue;
                    }
                }
                if (state[1]=='0' && state[3]=='1'){
                    if (neighbour[3]=='1'){
                        if (neighbour[5]!='0' || neighbour[7]!='1')
                            continue;
                    }
                    if (neighbour[3]=='0'){
                        if (neighbour[5]!='0' || neighbour[7]!='0')
                            continue;
                    }
                }

                if (state[2]=='1' && state[4]=='1'){
                    if(neighbour[4]!='1' || neighbour[6]!='0' || neighbour[8]!=state[8])
                        continue;
                }
                if (state[2]=='0' && state[4]=='0'){
                    if(neighbour[4]!='0' || neighbour[6]!=state[6] || neighbour[8]!='0')
                        continue;
                }
                if (state[2]=='1' && state[4]=='0'){
                    if (neighbour[4]=='1'){
                        if (neighbour[6]!='0' || neighbour[8]!='0')
                            continue;
                    }
                    if (neighbour[4]=='0'){
                        if (neighbour[6]!='1' || neighbour[8]!='0')
                            continue;
                    }
                }
                if (state[2]=='0' && state[4]=='1'){
                    if (neighbour[4]=='1'){
                        if (neighbour[6]!='0' || neighbour[8]!='1')
                            continue;
                    }
                    if (neighbour[4]=='0'){
                        if (neighbour[6]!='0' || neighbour[8]!='0')
                            continue;
                    }
                }
            }


            int neighbour_num = getIndex(state_list,neighbour);
            if (neighbour_num == 512){
                state_list.push_back(neighbour);
                neighbour_num = getIndex(state_list,neighbour);
            }
            std::stringstream ss;
            ss << neighbour_num;
            std::string str = ss.str();
            neighbour_list.append(str);
            neighbour_list.append(",");
        }
        if (neighbour_list.length() > 0){
            neighbour_list.pop_back();
            std::cout<<neighbour_list;
        }
        std::cout<<"\n";
        if (state_list.size() == current_state+1)
            break;
        current_state += 1;
    }

    std::cout <<"\n\n Vertex list \n";
    int n = 0;
    for (std::vector<std::string>::iterator ss = state_list.begin(); ss!= state_list.end(); ++ss){
        std::cout << n << ": " << *ss << "\n";
        n += 1;
    }
    
    return 1;
}
